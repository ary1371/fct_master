%% Testing of FCS model w.r.t. phi_1
%-----------------------------------------------------------------------------------------------------------
warning off;
corr_threshold = 0.99;
addpath('Configuration');

result_path = 'ResultsFCS';

if (exist(result_path) == 0)
    mkdir(result_path);
else
    rmdir(result_path, 's');
    mkdir(result_path);
end

%%
addpath('SampleTests');
addpath('FaultTable');


%% Original model
model = 'Models/FCS_fault_injector/sldemo_fuelsys.slx';
[ model_path, model_name_wo_ext, model_ext ] = fileparts( model );
model_path = [ model_path, '/' ];

% Load the system
system = load_system([model_path, model_name_wo_ext, model_ext]);

set_param(system, 'CovEnable', 'off')
clear covCumulativeData
set_param(system,'StopTime', '110', 'ReturnWorkspaceOutputs', 'on');
load('matlab.mat');
load('ModelConstantsThresholds_FTF.mat');

%% Model simulator and data accumulator

model_copy_name_wo_ext = model_name_wo_ext;
engine_speed1 = 300;
input_name = strcat(model_copy_name_wo_ext, '/w_d');
ST = 10;
Initial = 300;
Handle.Model = get_param('sldemo_fuelsys', 'Handle');
set(Handle.Model, 'StartTime','0','StopTime','110');
hws = get_param('sldemo_fuelsys', 'modelworkspace');

%% Run N simulations with random testing.

N = 10000;
for inum = 1:N
    
    IV = 0 + (620-0)*rand;
    FV = 0 + (620-0)*rand;
    
    set_param(input_name, 'Time', num2str(ST), 'Before', num2str(IV), 'After', num2str(FV));
    TestRT{inum} =  [ST IV FV];
    hws.assignin('speed_sw', 0);
    set_param(model_copy_name_wo_ext, 'SimulationCommand','update');
    
    % Run the simulation.
    simOut_wp = sim(model_copy_name_wo_ext, 'ReturnWorkspaceOutputs', 'on');
    simoutn_wp{inum} = simOut_wp;
    data{inum} = simOut_wp.get('sldemo_fuelsys_output');
    
    start_time = 0;
    stop_time = 110;
    dt = 0.01;
    time_var = (start_time:dt:stop_time)';
    signal_length = length(time_var);
    
    signalNames = getElementNames(data{inum}); % The Name should be assigned accordingly since a signal can have multiple data values in timeseries
    signal_names = getElementNames(data{inum});
    index = 1;
    for k = 1:length(getElementNames(data{inum}))
        logsoutElem(k) = get(data{inum},k);
        zero_add_above = 0;
        zero_add_below = 0;
        
        if (isprop(logsoutElem(k).Values,'Data'))
            logsoutData_woi = logsoutElem(k).Values.Data;
            [m_logsoutData_woi,n_logsoutData_woi] = size(logsoutData_woi);
            if (n_logsoutData_woi == 1)
                if (m_logsoutData_woi < signal_length)
                    if m_logsoutData_woi == 0
                        timevals = start_time:dt:stop_time;
                        for i = 1:length(timevals)
                            datavals = [];
                            datavals(i) = 0;
                        end
                        logsoutData_woi = cat(1,datavals',logsoutData_woi);
                    end
                    if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                        zero_add_above = uint64(logsoutElem(k).Values.Time(1)/dt);
                        logsoutData_fi = [];
                        logsoutData_fi(zero_add_above,1) = 0;
                        logsoutData_woi = cat(1,logsoutData_fi,logsoutData_woi);
                    end
                    if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(end) ~= stop_time)
                        zero_add_below = signal_length - m_logsoutData_woi - zero_add_above;
                        logsoutData_fi = [];
                        logsoutData_fi(zero_add_below,1) = 0;
                        logsoutData_woi = cat(1,logsoutData_woi,logsoutData_fi);
                    end
                    if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) == start_time) && (logsoutElem(k).Values.Time(2) ~= (start_time+dt))
                        val = logsoutElem(k).Values.Data(1);
                        timevals = start_time:dt:(logsoutElem(k).Values.Time(2)-dt);
                        for i = 1:length(timevals)
                            datavals = [];
                            datavals(i) = val;
                        end
                        logsoutData_woi = cat(1,datavals',logsoutData_woi(2:end));
                    end
                    if (m_logsoutData_woi == 1) && (logsoutElem(k).Values.Time(1) == start_time)
                        timevals = start_time+dt:dt:stop_time;
                        for i = 1:length(timevals)
                            datavals = [];
                            datavals(i) = 0;
                        end
                        logsoutData_woi = cat(1,datavals',logsoutData_woi);
                    end
                    logsoutData_array(:,index) = double(logsoutData_woi);
                else
                    logsoutData_array(:,index) = double(logsoutData_woi);
                end
                logsoutData_final{index} = logsoutData_woi;
                index = index + 1;
            end
        else
            logsoutData_struct = logsoutElem(k).Values;
            logsoutData_cell = struct2cell(logsoutData_struct);
            [m_logsoutData_cell,n_logsoutData_cell] = size(logsoutData_cell);
            for l = 1:m_logsoutData_cell
                logsoutData_w = logsoutData_cell{l}.Data;
                logsoutData_wi = logsoutData_cell{l}.Data;
                [m_logsoutData_wi,n_logsoutData_wi] = size(logsoutData_wi);
                if (m_logsoutData_wi < signal_length)
                    if (logsoutData_cell{1}.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                        zero_add_above = uint64(logsoutData_cell{1}.Time(1)/dt);
                        logsoutData_f = [];
                        logsoutData_f(zero_add_above,n_logsoutData_wi) = 0;
                        logsoutData_w = cat(1,logsoutData_f,logsoutData_wi);
                    end
                    if (logsoutData_cell{1}.Time(end) ~= stop_time)
                        zero_add_below = signal_length - uint64(logsoutData_cell{1}.Time(end)/dt) - zero_add_above;
                        logsoutData_f = [];
                        logsoutData_f(zero_add_below,n_logsoutData_wi) = 0;
                        logsoutData_w = cat(1,logsoutData_wi,logsoutData_f);
                    end
                end
                if (n_logsoutData_wi == 1)
                    logsoutData_array(:,index) = double(logsoutData_w);
                    logsoutData_final{index} = logsoutData_w;
                    index = index + 1;
                end
            end
        end
    end
    simout{inum} = logsoutData_array;
end
for m = 1:length(signal_names)
    idx = find(strcmp(names_table.ShortName, signal_names{m,1}));
    signal_names{m,2} = cell2mat(names_table.BlockIndex(idx));
    signal_names{m,3} = cell2mat(names_table.SignalType(idx));
end


%% Compute robustness of each simulation trace w.r.t. STL property phi
for i = 1:N
    s1 = simout{1,i};
    rob = monitorFTF1(s1);
    Robustness{i} = rob;
end


%% Perform cross-correlation on simulated data to eliminate signals with high correlation
% [ keep_sig ] = corrcom( simout, corr_threshold );
disp('corrcom.m: Computing cross correlation matrices');

% Get the number of simulations
nb_sim = length(simout);

% We assume that each simulation has the same number of signals and samples
% We now get the number of signals
[nb_samples nb_signals] = size(simout{1});

% Matrix of nb_signals x nb_signals for the cross-correlation analysis, initalized to 0

% We compute the correlation coefficients
for i = 1:nb_sim
    variance{i} = var(simout{i});
    covariance{i} = cov(simout{i});
    dispersion{i} = diag(covariance{i});
    std_dev{i} = sqrt(dispersion{i});
    corr_coeff = corrcoef(simout{i});
    corr_coeffs{i} = corr_coeff;
end

% We now have nb_sims correlation coefficient matrices,
% each of size nb_signals x nb_signals
% We now create a single nb_signals x nb_signals matrix
% where each (i,j) cell is the minimum (worst case)
% of the individual simulation (i,j) cells
min_corr_matrix = ones(nb_signals,nb_signals);
for i = 1:nb_sim
    min_corr_matrix = min(min_corr_matrix, abs(corr_coeffs{i}), 'includenan');
end

% Now we have our correlation coefficient matrix summary
% We remove all signals sj that have correlation to some si
% in [0.99,1]
keep_sig = ones(1,nb_signals);
for i = 2:nb_signals
    for j = 1:i-1
        if (min_corr_matrix(i,j) >= corr_threshold && min_corr_matrix(i,j) <= 1)
            keep_sig(i) = 0;
        end
    end
end

%% Remove the input and output signals from the list of reduced signals
new_keep_signal = keep_sig;
IO = {'s93','s70','s80','s85','s86','s82','s81','s77','s74','s76','s73','s71','s90','s89'};
for i = 1:length(keep_sig)
    if keep_sig(i) == 1
        if max(strcmp(IO,signalNames(i)))
            new_keep_signal(i) = 0;
        end
    end
end

%% Check correlation between each internal signal and respective output
for k = 1:length(new_keep_signal)
    if new_keep_signal(k) == 1 % if signal is internal
        % compute the correlation coefficient between the internal signal and the outputs signal
        for i = 1:N
            correlationcoeffop1 = corrcoef(simout{1,i}(:,k), simout{1,i}(:,5)); % w.r.t. op1 air-fuel-ratio
            correlationcoeffsop1{1,k}{1,i} = correlationcoeffop1;
            corrval{1,k}{1,i} =  correlationcoeffop1(1,2);
        end
        % Compute the mean value of correlation coeff
        meancorr{1,k} = mean(cell2mat(corrval{1,k}));
        maxcorr{1,k} = max(cell2mat(corrval{1,k}));
    end
end

%% If max correlation coeff falls below a threshold (set to 0.5), we exclude the corresponding signal from analysis

significant_signal = new_keep_signal;
for k = 1:length(new_keep_signal)
    if new_keep_signal(k) == 1
        if abs(maxcorr{1,k}) < 0.5 || isnan(maxcorr{1,k})
            significant_signal(k) = 0;
        end
    end
end

% Number of significant signals
Num_Significant = sum(significant_signal);

%% Identify what features are relevant for each significant signal
% correlation check between each feature value and robustness value
t = time_var;
feature_correlation_threshold = 0.5;
for g = 1:length(significant_signal)
    if significant_signal(g) == 1 % if signal is significant, compute the feature for each simulation
        for i = 1:N
            %% feature 1: Mean
            avgval = mean(simout{1,i}(:,g));
            mean_val{1,g}{1,i} = avgval;
            
            %% feature 2: Standard Deviation
            stddev = std(simout{1,i}(:,g));
            std_val{1,g}{1,i} = stddev;
            
            %% feature 3: Root Mean Square
            rmsval = rms(simout{1,i}(:,g));
            rms_val{1,g}{1,i} = rmsval;
            
            %% feature 4: Shape factor
            meanabs = mean(abs(simout{1,i}(:,g)));
            shapefactor{1,g}{1,i} = rmsval/meanabs;
            
            %% feature 5: Kurtosis
            kurtosisval = kurtosis(simout{1,i}(:,g));
            kurtosis_val{1,g}{1,i} = kurtosisval;
            
            %% feature 6: Skewness
            skewnessval = skewness(simout{1,i}(:,g));
            skewness_val{1,g}{1,i} = skewnessval;
            
            %% feature 7: Peak Value
            peakval = max(abs(simout{1,i}(:,g)));
            peak_val{1,g}{1,i} = peakval;
            
            %% feature 8: Impulse factor
            impulsefactor{1,g}{1,i} = peakval/meanabs;
            
            %% feature 9: Crest factor
            crestfactor{1,g}{1,i} = peak2rms(simout{1,i}(:,g));
            
            %% feature 10: Peak Instantaneous Frequency
            instf = instfreq(simout{1,i}(:,g),t);
            maxinstf{1,g}{1,i} = max(instf);
        end
    end
end

mean_feature = significant_signal;
sd_feature = significant_signal;
rms_feature = significant_signal;
sf_feature = significant_signal;
kurt_feature = significant_signal;
skew_feature = significant_signal;
peak_feature = significant_signal;
if_feature = significant_signal;
cf_feature = significant_signal;
minstf_feature = significant_signal;


for g = 1:length(significant_signal)
    if significant_signal(g) == 1
        % check corr coeff between each feature vector value and robustness
        % vector value
        corr_mean{1,g} = corrcoef(cell2mat(mean_val{1,g}),cell2mat(Robustness));
        
        corr_sd{1,g} = corrcoef(cell2mat(std_val{1,g}),cell2mat(Robustness));
        
        corr_rms{1,g} = corrcoef(cell2mat(rms_val{1,g}),cell2mat(Robustness));
        
        corr_sf{1,g} = corrcoef(cell2mat(shapefactor{1,g}),cell2mat(Robustness));
        
        corr_kurt{1,g} = corrcoef(cell2mat(kurtosis_val{1,g}),cell2mat(Robustness));
        
        corr_skew{1,g} = corrcoef(cell2mat(skewness_val{1,g}),cell2mat(Robustness));
        
        corr_peak{1,g} = corrcoef(cell2mat(peak_val{1,g}),cell2mat(Robustness));
        
        corr_if{1,g} = corrcoef(cell2mat(impulsefactor{1,g}),cell2mat(Robustness));
        
        corr_cf{1,g} = corrcoef(cell2mat(crestfactor{1,g}),cell2mat(Robustness));
        
        corr_minstf{1,g} = corrcoef(cell2mat(maxinstf{1,g}),cell2mat(Robustness));
    end
end

% remove the feature if high correlation is not observed
for g = 1:length(significant_signal)
    if significant_signal(g) == 1
        if abs(corr_mean{1,g}(1,2)) < feature_correlation_threshold
            mean_feature(g) = 0;
        end
        if abs(corr_sd{1,g}(1,2)) < feature_correlation_threshold
            sd_feature(g) = 0;
        end
        if abs(corr_rms{1,g}(1,2)) < feature_correlation_threshold
            rms_feature(g) = 0;
        end
        if abs(corr_sf{1,g}(1,2)) < feature_correlation_threshold
            sf_feature(g) = 0;
        end
        if abs(corr_kurt{1,g}(1,2)) < feature_correlation_threshold
            kurt_feature(g) = 0;
        end
        if abs(corr_skew{1,g}(1,2)) < feature_correlation_threshold
            skew_feature(g) = 0;
        end
        if abs(corr_peak{1,g}(1,2)) < feature_correlation_threshold
            peak_feature(g) = 0;
        end
        if abs(corr_if{1,g}(1,2)) < feature_correlation_threshold
            if_feature(g) = 0;
        end
        if abs(corr_cf{1,g}(1,2)) < feature_correlation_threshold
            cf_feature(g) = 0;
        end
        if abs(corr_minstf{1,g}(1,2)) < feature_correlation_threshold
            minstf_feature(g) = 0;
        end
    end
end

% signals for which the feature is chosen
sig_mean = find(mean_feature > 0);
sig_sd = find(sd_feature > 0);
sig_rms = find(rms_feature > 0);
sig_sf = find(sf_feature > 0);
sig_kurt = find(kurt_feature > 0);
sig_skew = find(skew_feature > 0);
sig_peak = find(peak_feature > 0);
sig_if = find(if_feature > 0);
sig_cf = find(cf_feature > 0);
sig_minstf = find(minstf_feature > 0);


%% example coverage computation: all targets with random testing
% Remove the input and output signals from the list of reduced signals
Internal_signal = ones(1,nb_signals);
IO = {'s93','s70','s80','s85','s86','s82','s81','s77','s74','s76','s73','s71','s90','s89'};
for i = 1:length(Internal_signal)
    if max(strcmp(IO,signalNames(i)))
        Internal_signal(i) = 0;
    end
end

% Find coverage for all features of all internal signals
for g = 1:length(Internal_signal)
    if Internal_signal(g) == 1
        for i = 1:N
            
            %% feature 1: Mean
            avgval = mean(simout{1,i}(:,g));
            mean_val{1,g}{1,i} = avgval;
            mean_valu = cell2mat(mean_val{1,g});
            u = unique(mean_valu);
            [Nmean{1,g},edgesmean{1,g}] = histcounts(u);
            [rowmean,colmean] = find((Nmean{1,g}) > 0 );
            BinOccupancymean = sum(rowmean);
            NoofBinsmeanRandom{1,g} = length((Nmean{1,g}));
            
            %% feature 2: Standard Deviation
            stddev = std(simout{1,i}(:,g));
            std_val{1,g}{1,i} = stddev;
            std_valu = cell2mat(std_val{1,g});
            u = unique(std_valu);
            [Nsd{1,g},edgessd{1,g}] = histcounts(u);
            [rowsd,colmsd] = find((Nsd{1,g}) > 0 );
            BinOccupancysd = sum(rowsd);
            NoofBinssdRandom{1,g} = length((Nsd{1,g}));
            
            %% feature 3: Root Mean Square
            rmsval = rms(simout{1,i}(:,g));
            rms_val{1,g}{1,i} = rmsval;
            rms_valu = cell2mat(rms_val{1,g});
            u = unique(rms_valu);
            [Nrms{1,g},edgesrms{1,g}] = histcounts(u);
            [rowrms,colrms] = find((Nrms{1,g}) > 0 );
            BinOccupancyrms = sum(rowrms);
            NoofBinsrmsRandom{1,g} = length((Nrms{1,g}));
            
            %% feature 4: Shape factor
            meanabs = mean(abs(simout{1,i}(:,g)));
            shapefactor{1,g}{1,i} = rmsval/meanabs;
            shapefactor1 = cell2mat(shapefactor{1,g});
            u = unique(shapefactor1);
            [Nsf{1,g},edgessf{1,g}] = histcounts(u);
            [rowsf,colsf] = find((Nsf{1,g}) > 0 );
            BinOccupancysf = sum(rowsf);
            NoofBinssfRandom{1,g} = length((Nsf{1,g}));
            
            %% feature 5: Kurtosis
            kurtosisval = kurtosis(simout{1,i}(:,g));
            kurtosis_val{1,g}{1,i} = kurtosisval;
            kurtosis_valu = cell2mat(kurtosis_val{1,g});
            u = unique(kurtosis_valu);
            [Nku{1,g},edgesku{1,g}] = histcounts(u);
            [rowku,colmku] = find((Nku{1,g}) > 0 );
            BinOccupancyku = sum(rowku);
            NoofBinskuRandom{1,g} = length((Nku{1,g}));
            
            %% feature 6: Skewness
            skewnessval = skewness(simout{1,i}(:,g));
            skewness_val{1,g}{1,i} = skewnessval;
            skewness_valu = cell2mat(skewness_val{1,g});
            u = unique(skewness_valu);
            [Nsk{1,g},edgessk{1,g}] = histcounts(u);
            [rowsk,colmsk] = find((Nsk{1,g}) > 0 );
            BinOccupancysk = sum(rowsk);
            NoofBinsskRandom{1,g} = length((Nsk{1,g}));
            
            %% feature 7: Peak Value
            peakval = max(abs(simout{1,i}(:,g)));
            peak_val{1,g}{1,i} = peakval;
            peak_valu = cell2mat(peak_val{1,g});
            u = unique(peak_valu);
            [Npeak{1,g},edgespeak{1,g}] = histcounts(u);
            [rowpeak,colmpeak] = find((Npeak{1,g}) > 0 );
            BinOccupancypeak = sum(rowpeak);
            NoofBinspeakRandom{1,g} = length((Npeak{1,g}));
            
            %% feature 8: Impulse factor
            impulsefactor{1,g}{1,i} = peakval/meanabs;
            impulsefactor1 = cell2mat(impulsefactor{1,g});
            u = unique(impulsefactor1);
            [Nif{1,g},edgesif{1,g}] = histcounts(u);
            [rowif,colmif] = find((Nif{1,g}) > 0 );
            BinOccupancyif = sum(rowif);
            NoofBinsifRandom{1,g} = length((Nif{1,g}));
            
            %% feature 9: Crest factor
            crestfactor{1,g}{1,i} = peak2rms(simout{1,i}(:,g));
            crestfactor1 = cell2mat(crestfactor{1,g});
            u = unique(crestfactor1);
            [Ncf{1,g},edgescf{1,g}] = histcounts(u);
            [rowcf,colmcf] = find((Ncf{1,g}) > 0 );
            BinOccupancycf = sum(rowcf);
            NoofBinscfRandom{1,g} = length((Ncf{1,g}));
            
            %% feature 10: Peak Instantaneous Frequency
            instf = instfreq(simout{1,i}(:,g),t);
            maxinstf{1,g}{1,i} = max(instf);
            maxinst = cell2mat(maxinstf{1,g});
            u = unique(maxinst);
            [Nmif{1,g},edgesmif{1,g}] = histcounts(u);
            [rowmif,colmmif] = find((Nmif{1,g}) > 0 );
            BinOccupancymif = sum(rowmif);
            NoofBinsmifRandom{1,g} = length((Nmif{1,g}));
            
        end
    end
end

% Capture all coverage metrics for all internal signals of the system
zeta_randomall = [NoofBinscfRandom NoofBinsifRandom NoofBinskuRandom NoofBinsmeanRandom NoofBinsmifRandom NoofBinspeakRandom NoofBinsrmsRandom NoofBinssdRandom NoofBinssfRandom NoofBinsskRandom];
val_zeta_extendedall = cell2mat(zeta_randomall);

%% MUTATION TESTING EXPERIMENTS: Property-based mutation testing (PBMT)
warning('off','all');

%% Initialization

addpath('FaultInjector_Master');
load_system('FInjLib');
set_param('FInjLib', 'Lock', 'off');

%% Add folders to path and load the initial test suite

addpath('FCS_fault_injector');
addpath('Test Generation')
addpath('ResultsFCS');
addpath('Configuration');
load('matlab.mat');
load('ModelConstantsThresholds_FTF.mat');
load('Testgen.mat');

%% Test suite
TSuite = [TestART];

for gg = 1%:length(TSuite) % for all the test suites, do the following
    
    Testcase_init = TSuite;
    
    %% Simulate the original model
    for inum = 1:length(Testcase_init)
        ST =  num2str(Testcase_init{1,inum}(1));
        IV =  num2str(Testcase_init{1,inum}(2));
        FV = num2str(Testcase_init{1,inum}(3));
        set_param(input_name, 'Time', num2str(ST), 'Before', num2str(IV), 'After', num2str(FV));
        hws.assignin('speed_sw', 0);
        set_param(model_copy_name_wo_ext, 'SimulationCommand','update');
        simOut_orig = sim(model_copy_name_wo_ext, 'ReturnWorkspaceOutputs', 'on');
        simOutorig{inum} = simOut_orig;
        data_orig{inum} = simOut_orig.get('sldemo_fuelsys_output');
        
        start_time = 0;
        stop_time = 110;
        dt = 0.01;
        time_var = (start_time:dt:stop_time)';
        signal_length = length(time_var);
        
        signalNames = getElementNames(data_orig{inum}); % The Name should be assigned accordingly since a signal can have multiple data values in timeseries
        signal_names = getElementNames(data_orig{inum});
        index = 1;
        for k = 1:length(getElementNames(data_orig{inum}))
            logsoutElem(k) = get(data_orig{inum},k);
            zero_add_above = 0;
            zero_add_below = 0;
            
            if (isprop(logsoutElem(k).Values,'Data'))
                logsoutData_woi = logsoutElem(k).Values.Data;
                [m_logsoutData_woi,n_logsoutData_woi] = size(logsoutData_woi);
                if (n_logsoutData_woi == 1)
                    if (m_logsoutData_woi < signal_length)
                        if m_logsoutData_woi == 0
                            timevals = start_time:dt:stop_time;
                            for i = 1:length(timevals)
                                datavals = [];
                                datavals(i) = 0;
                            end
                            logsoutData_woi = cat(1,datavals',logsoutData_woi);
                        end
                        if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                            zero_add_above = uint64(logsoutElem(k).Values.Time(1)/dt);
                            logsoutData_fi = [];
                            logsoutData_fi(zero_add_above,1) = 0;
                            logsoutData_woi = cat(1,logsoutData_fi,logsoutData_woi);
                        end
                        if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(end) ~= stop_time)
                            zero_add_below = signal_length - m_logsoutData_woi - zero_add_above;
                            logsoutData_fi = [];
                            logsoutData_fi(zero_add_below,1) = 0;
                            logsoutData_woi = cat(1,logsoutData_woi,logsoutData_fi);
                        end
                        if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) == start_time) && (logsoutElem(k).Values.Time(2) ~= (start_time+dt))
                            val = logsoutElem(k).Values.Data(1);
                            timevals = start_time:dt:(logsoutElem(k).Values.Time(2)-dt);
                            for i = 1:length(timevals)
                                datavals = [];
                                datavals(i) = val;
                            end
                            logsoutData_woi = cat(1,datavals',logsoutData_woi(2:end));
                        end
                        if (m_logsoutData_woi == 1) && (logsoutElem(k).Values.Time(1) == start_time)
                            timevals = start_time+dt:dt:stop_time;
                            for i = 1:length(timevals)
                                datavals = [];
                                datavals(i) = 0;
                            end
                            logsoutData_woi = cat(1,datavals',logsoutData_woi);
                        end
                        logsoutData_array(:,index) = double(logsoutData_woi);
                    else
                        logsoutData_array(:,index) = double(logsoutData_woi);
                    end
                    logsoutData_final{index} = logsoutData_woi;
                    index = index + 1;
                end
            else
                logsoutData_struct = logsoutElem(k).Values;
                logsoutData_cell = struct2cell(logsoutData_struct);
                [m_logsoutData_cell,n_logsoutData_cell] = size(logsoutData_cell);
                for l = 1:m_logsoutData_cell
                    logsoutData_w = logsoutData_cell{l}.Data;
                    logsoutData_wi = logsoutData_cell{l}.Data;
                    [m_logsoutData_wi,n_logsoutData_wi] = size(logsoutData_wi);
                    if (m_logsoutData_wi < signal_length)
                        if (logsoutData_cell{1}.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                            zero_add_above = uint64(logsoutData_cell{1}.Time(1)/dt);
                            logsoutData_f = [];
                            logsoutData_f(zero_add_above,n_logsoutData_wi) = 0;
                            logsoutData_w = cat(1,logsoutData_f,logsoutData_wi);
                        end
                        if (logsoutData_cell{1}.Time(end) ~= stop_time)
                            zero_add_below = signal_length - uint64(logsoutData_cell{1}.Time(end)/dt) - zero_add_above;
                            logsoutData_f = [];
                            logsoutData_f(zero_add_below,n_logsoutData_wi) = 0;
                            logsoutData_w = cat(1,logsoutData_wi,logsoutData_f);
                        end
                    end
                    if (n_logsoutData_wi == 1)
                        logsoutData_array(:,index) = double(logsoutData_w);
                        logsoutData_final{index} = logsoutData_w;
                        index = index + 1;
                    end
                end
            end
        end
        simout_orig = logsoutData_array;
        SOut_orig{inum} = simout_orig;
        rob_orig{inum} = monitorFTF1(simout_orig);
    end
    SOut_orig = [SOut_orig];
    out_Original = SOut_orig;
    ROB_ORIG = [rob_orig];
    
    %% Simulate the mutants (i.e., the faulty versions)
    % Gather mutant information
    newtable = readtable(strcat('FaultTable/ResultsFCS', '/Fault_table.xlsx'));
    
    % Mutant execution
    for pq = 1 : height(newtable) % for all mutants in the fault table, do the following
        modelname = cell2mat(table2array(newtable(pq,1)));
        
        % Load the system
        system = load_system([model_path, modelname, model_ext]);
        set_param(system, 'AutoInsertRateTranBlk','on');
        set_param(system, 'StopTime', '110', 'ReturnWorkspaceOutputs', 'on');
        input_name = strcat(modelname,'/w_d');
        Handle.Model = get_param(modelname, 'Handle');
        set(Handle.Model, 'StartTime','0','StopTime','110');
        
        hws = get_param(modelname, 'modelworkspace');
        
        for i = 1:length(Testcase_init)
            ST =  num2str(Testcase_init{1,i}(1));
            IV =  num2str(Testcase_init{1,i}(2));
            FV = num2str(Testcase_init{1,i}(3));
            set_param(input_name, 'Time', num2str(ST), 'Before', num2str(IV), 'After', num2str(FV));
            hws.assignin('speed_sw', 0);
            set_param(modelname, 'SimulationCommand','update');
            Simulink.SuppressedDiagnostic(strcat(modelname,'/Engine Gas Dynamics/Throttle & Manifold/Intake Manifold/MATLAB Function'),'Simulink:Engine:BlockOutputInfNanDetectedError');
            %Simulink.SuppressedDiagnostic(strcat(modelname,'/Engine Gas Dynamics/Throttle & Manifold/Intake Manifold/Sum'),'Simulink:Engine:BlockOutputInfNanDetectedError');
            Simulink.SuppressedDiagnostic(strcat(modelname,'/Engine Gas Dynamics/Throttle & Manifold/Intake Manifold/RT//Vm'),'Simulink:Engine:BlockOutputInfNanDetectedError');
            Simulink.SuppressedDiagnostic(strcat(modelname,'/To Controller'),'Simulink:Engine:BlockOutputInfNanDetectedError');
            Simulink.SuppressedDiagnostic(strcat(modelname,'/Engine Gas Dynamics/Mixing & Combustion/Product'),'Simulink:Engine:BlockOutputInfNanDetectedError');
            simOutmutant{i} = sim(modelname, 'ReturnWorkspaceOutputs', 'on');
        end
        save_system(system);
        out_mutant{pq} = simOutmutant;
    end
    SOut_mut{gg} = out_mutant; % Store Simulation output of mutants for test suite number 'gg'
    
    %% Evaluate simulation data to compute mutation scores
    
    for pq = 1:height(newtable)
        for nb_test = 1:length(out_mutant{1,pq})
            data = out_mutant{1,pq}{1,nb_test}.get('sldemo_fuelsys_output');
            data_mutant{pq}{nb_test} = data;
            signalNames_mut{pq}{nb_test} = getElementNames(data);
        end
    end
    
    datamut = data_mutant;
    signalNames_mutant = signalNames_mut;
    
    for l = 1 : height(newtable) % mutant number
        for  j = 1:length(out_mutant{1,l})
            simdata_mut{1,l}{1,j} = find(datamut{1,l}{1, j},'Name','s70');
        end
    end
    
    %% ----------------------------------------------------------------------------------
    %% Code for fetching and analyzing simulation data for PBMT (property-based mutation testing)
    
    %% Mutants (Faulty models): Compute robustness values for all test cases
    % Computing robustness of all mutants for every test case
    for pq = 1:height(newtable) % for each mutant
        for nb_test = 1:length(out_mutant{1,pq}) % for each test case
            data = simdata_mut{pq}{nb_test};
            signalNames = signalNames_mutant{pq}{nb_test};
            start_time = 0;
            stop_time = 110;
            dt = 0.01;
            index = 1;
            time_var = (start_time:dt:stop_time)';
            signal_length = length(time_var);
            for k = 1:length(getElementNames(data))
                logsoutElem(k) = get(data,k);
                zero_add_above = 0;
                zero_add_below = 0;
                logsoutName = logsoutElem(k).Values.Name; % Not all Values may have Names for Data
                if (isprop(logsoutElem(k).Values,'Data'))
                    logsoutData_woi = logsoutElem(k).Values.Data;
                    [m_logsoutData_woi,n_logsoutData_woi] = size(logsoutData_woi);
                    if (n_logsoutData_woi == 1)
                        if (m_logsoutData_woi < signal_length)
                            if m_logsoutData_woi == 0
                                timevals = start_time:dt:stop_time;
                                for i = 1:length(timevals)
                                    datavals = [];
                                    datavals(i) = 0;
                                end
                                logsoutData_woi = cat(1,datavals',logsoutData_woi);
                            end
                            if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                                zero_add_above = uint64(logsoutElem(k).Values.Time(1)/dt);
                                logsoutData_fi = [];
                                logsoutData_fi(zero_add_above,1) = 0;
                                logsoutData_woi = cat(1,logsoutData_fi,logsoutData_woi);
                            end
                            if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(end) ~= stop_time)
                                zero_add_below = signal_length - m_logsoutData_woi - zero_add_above;
                                logsoutData_fi = [];
                                logsoutData_fi(zero_add_below,1) = 0;
                                logsoutData_woi = cat(1,logsoutData_woi,logsoutData_fi);
                            end
                            if (m_logsoutData_woi > 1) && (logsoutElem(k).Values.Time(1) == start_time) && (logsoutElem(k).Values.Time(2) ~= (start_time+dt))
                                val = logsoutElem(k).Values.Data(1);
                                timevals = start_time:dt:(logsoutElem(k).Values.Time(2)-dt);
                                for i = 1:length(timevals)
                                    datavals = [];
                                    datavals(i) = val;
                                end
                                logsoutData_woi = cat(1,datavals',logsoutData_woi(2:end));
                            end
                            if (m_logsoutData_woi == 1) && (logsoutElem(k).Values.Time(1) == start_time)
                                timevals = start_time+dt:dt:stop_time;
                                for i = 1:length(timevals)
                                    datavals = [];
                                    datavals(i) = 0;
                                end
                                logsoutData_woi = cat(1,datavals',logsoutData_woi);
                            end
                            logsoutData_array(:,index) = double(logsoutData_woi);
                        else
                            logsoutData_array(:,index) = double(logsoutData_woi);
                        end
                        logsoutData_final{index} = logsoutData_woi;
                        index = index + 1;
                    end
                else
                    logsoutData_struct = logsoutElem(k).Values;
                    logsoutData_cell = struct2cell(logsoutData_struct);
                    [m_logsoutData_cell,n_logsoutData_cell] = size(logsoutData_cell);
                    for l = 1:m_logsoutData_cell
                        logsoutData_w = logsoutData_cell{l}.Data;
                        logsoutData_wi = logsoutData_cell{l}.Data;
                        [m_logsoutData_wi,n_logsoutData_wi] = size(logsoutData_wi);
                        if (m_logsoutData_wi < signal_length)
                            if (logsoutData_cell{1}.Time(1) ~= start_time) % Append zeros to the signal in the beginning or end or both as necessary
                                zero_add_above = uint64(logsoutData_cell{1}.Time(1)/dt);
                                logsoutData_f = [];
                                logsoutData_f(zero_add_above,n_logsoutData_wi) = 0;
                                logsoutData_w = cat(1,logsoutData_f,logsoutData_wi);
                            end
                            if (logsoutData_cell{1}.Time(end) ~= stop_time)
                                zero_add_below = signal_length - uint64(logsoutData_cell{1}.Time(end)/dt) - zero_add_above;
                                logsoutData_f = [];
                                logsoutData_f(zero_add_below,n_logsoutData_wi) = 0;
                                logsoutData_w = cat(1,logsoutData_wi,logsoutData_f);
                            end
                        end
                        if (n_logsoutData_wi == 1)
                            logsoutData_array(:,index) = double(logsoutData_w);
                            logsoutData_final{index} = logsoutData_w;
                            index = index + 1;
                        end
                    end
                end
            end
            simout = logsoutData_array;
            SIMOUT_Mutant{pq}{nb_test} = simout;
            rob_mut{pq}{nb_test} = monitorFTF1(simout);
        end
    end
    
    ROB_MUT{gg} = rob_mut;
    
    %% ----------------------------------------------------------------------------------
    %% PBMT : Property-based mutation testing of FCS
    
    %% Finding the killed mutants
    % Strong mutation testing with RIPR model
    % use the passing/failing verdicts of the test cases w.r.t. phi
    
    %% Make rob value 1 if positive; otherwise 0
    % for original model
    bool_rob_orig = double(cell2mat(ROB_ORIG) > 0);
    % for mutants
    for nb_test = 1:length(out_Original)
        for mutantnum = 1:height(newtable)
            bool_rob_mut(nb_test,mutantnum) = double((rob_mut{1,mutantnum}{1,nb_test}) > 0);
        end
    end
    
    %% Find the phi-killed mutants
    for i = 1:length(data_orig) % for all test cases
        for mutantnum = 1:height(newtable) % for all mutants
            % phi-kill the mutant if it the test passes on the original model
            % but fails on the mutant
            % i.e., if bool_rob_orig == 1 and bool_rob_mut == 0
            if bool_rob_orig(i) == 1 && bool_rob_mut(i,mutantnum) == 0
                phi_kill = 1;
            else
                phi_kill = 0;
            end
            phi_kill_info(i,mutantnum) = phi_kill;
        end
    end
    
    phiKilledmutant_info{gg} = phi_kill_info;
    
    for mutantnum = 1:height(newtable) % for all mutants
        % count the number of tests that kill a mutant
        ntkm_phi(1,mutantnum) = sum(phi_kill_info(:,mutantnum));% number of tests that kill the mutant
        if ntkm_phi(1,mutantnum) > 0
            mutant_phikilled(1,mutantnum) = 1;% 1 if the mutant is killed by atleast one test
        else
            mutant_phikilled(1,mutantnum) = 0;
        end
    end
    
    Mutant_phikilled_analysis{gg} = mutant_phikilled;
    
    phi_KM = sum(mutant_phikilled); % Number of phi-killed mutants
    phi_killablemut = 46; % Number of phi-killable mutants
    MS_percentage = (phi_KM/phi_killablemut)*100;
    
    PBMT_KM(gg) = phi_KM;
    PBMT_MS_percentage(gg) = MS_percentage;
end
