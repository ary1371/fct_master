# FCT

This repository includes the tools, experimental subjects, the experimental data and scripts. Please visit the following repository for full data and scripts: https://gitlab.com/DrishtiYadav/fct_master

# Installation
To run our experiments, you will need a valid license of MATLAB from MathWorks. We recommend downloading and installing the full package of MATLAB (R2018b) with Simulink.

Additionally, please install the following packages:

(1) [RTAMT](https://github.com/nickovic/rtamt)
(2) [FIM](https://gitlab.com/DrishtiYadav/fimtool)

# Details
The system Simulink models are stored in 'Models' along with the mutants. Additionally, we have the folder 'FaultTable', which include fault tables providing detailed descriptions of all the generated mutants of the models, respectively.

All the results reported in the paper and the scripts to generate the plots are stored in the folder 'ResultData'.

# Requisites
Create a copy of the 'FaultInjector_Master' folder from the [FIM](https://gitlab.com/DrishtiYadav/fimtool) package to the main directory of FCT.

Since [FIM](https://gitlab.com/DrishtiYadav/fimtool) is created in R2020b and we perform the experiments in R2018b, you first need to do the following to ensure compatibility: 

(1) Open MATLAB R2020b.
(2) Navigate through the folder ‘FaultInjector_Master’.
(3) Open 'FInjLib.slx'.
(4) In the Simulink Editor, on the Simulation tab, select Save > Previous Version. In the Export to Previous Version dialog box, from the Save as type list, select the version R2018b to which we export the model.
(5) Click Save.
(6) Close MATLAB R2020b.

Then, open MATLAB R2018b. You can now proceed with conducting the experiments.
 
## Testing
To test a model named x for a given property y, run the script 'x_FCTy.m'. 
For example, for testing with ATCS for its $\phi_1$: run the script 'ATCS_FCT1.m'

Follow the same steps for the other examples.
All the results will be stored (as variables) in the MATLAB Workspace. 
