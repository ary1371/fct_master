%% Boxplots of different models at different snapshots of time

%% FCS Model
load("FCS.mat");

figure;
clf()
ax = gobjects(3,1);

ax(1) = subplot(1,3,1);
color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55])];

group = [    ones(size(ART_FCS_1k'));
         2 * ones(size(VB_OD_FCS_1k'));
         3 * ones(size(FB_OD_FCS_1k'));
         4 * ones(size(FC_FCS_1k'));
         5 * ones(size(FO_FCS1_1k'));
         6 * ones(size(FO_FCS2_1k'))];


bh1 = boxplot([ART_FCS_1k'; VB_OD_FCS_1k'; FB_OD_FCS_1k'; FC_FCS_1k'; FO_FCS1_1k'; FO_FCS2_1k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(1),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_FCS_1k), mean(VB_OD_FCS_1k), mean(FB_OD_FCS_1k), mean(FC_FCS_1k), mean(FO_FCS1_1k), mean(FO_FCS2_1k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j =1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end


%% data of aggregate signal feature coverage at 8k seconds for 30 runs

ax(2)=subplot(1,3,2);
group = [    ones(size(ART_FCS_8k'));
         2 * ones(size(VB_OD_FCS_8k'));
         3 * ones(size(FB_OD_FCS_8k'));
         4 * ones(size(FC_FCS_8k'));
         5 * ones(size(FO_FCS1_8k'));
         6 * ones(size(FO_FCS2_8k'))];


bh1 = boxplot([ART_FCS_8k'; VB_OD_FCS_8k'; FB_OD_FCS_8k'; FC_FCS_8k'; FO_FCS1_8k'; FO_FCS2_8k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(2),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_FCS_8k), mean(VB_OD_FCS_8k), mean(FB_OD_FCS_8k), mean(FC_FCS_8k), mean(FO_FCS1_8k), mean(FO_FCS2_8k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j = 1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j = 1:length(h)
    set(h(j),'Visible','off');
end

%% data of aggregate signal feature coverage at 16k seconds for 30 runs

ax(3)=subplot(1,3,3);

group = [    ones(size(ART_FCS_16k'));
         2 * ones(size(VB_OD_FCS_16k'));
         3 * ones(size(FB_OD_FCS_16k'));
         4 * ones(size(FC_FCS_16k'));
         5 * ones(size(FO_FCS1_16k'));
         6 * ones(size(FO_FCS2_16k'))];


bh1 = boxplot([ART_FCS_16k'; VB_OD_FCS_16k'; FB_OD_FCS_16k'; FC_FCS_16k'; FO_FCS1_16k'; FO_FCS2_16k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});

xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box

set(bh1,'LineWidth',1.5);
axis(ax(3),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_FCS_16k), mean(VB_OD_FCS_16k), mean(FB_OD_FCS_16k), mean(FC_FCS_16k), mean(FO_FCS1_16k), mean(FO_FCS2_16k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
set(objs,'LineStyle',lst(j,1));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end

linkaxes(ax, 'y')

%% ATCS model
load("ATCS.mat");
figure;
clf()
ax = gobjects(3,1);

ax(1)=subplot(1,3,1);
color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.60 0.54 0.65])];


group = [    ones(size(ART_ATCS_1k'));
         2 * ones(size(VB_OD_ATCS_1k'));
         3 * ones(size(FB_OD_ATCS_1k'));
         4 * ones(size(FC_ATCS_1k'));
         5 * ones(size(FO_ATCS1_1k'));
         6 * ones(size(FO_ATCS2_1k'));
         7 * ones(size(FO_ATCS3_1k'));
         8 * ones(size(FO_ATCS4_1k'));];


bh1 = boxplot([ART_ATCS_1k'; VB_OD_ATCS_1k'; FB_OD_ATCS_1k'; FC_ATCS_1k'; FO_ATCS1_1k'; FO_ATCS2_1k';FO_ATCS3_1k'; FO_ATCS4_1k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2', 'FCT-S:3', 'FCT-S:4'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(1),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_ATCS_1k), mean(VB_OD_ATCS_1k), mean(FB_OD_ATCS_1k), mean(FC_ATCS_1k), mean(FO_ATCS1_1k), mean(FO_ATCS2_1k), mean(FO_ATCS3_1k), mean(FO_ATCS4_1k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j =1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-.";":";"--"]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end


%% data of aggregate signal feature coverage at 8k seconds for 30 runs

ax(2)=subplot(1,3,2);

group = [    ones(size(ART_ATCS_8k'));
         2 * ones(size(VB_OD_ATCS_8k'));
         3 * ones(size(FB_OD_ATCS_8k'));
         4 * ones(size(FC_ATCS_8k'));
         5 * ones(size(FO_ATCS1_8k'));
         6 * ones(size(FO_ATCS2_8k'));
         7 * ones(size(FO_ATCS3_8k'));
         8 * ones(size(FO_ATCS4_8k'));];


bh1 = boxplot([ART_ATCS_8k'; VB_OD_ATCS_8k'; FB_OD_ATCS_8k'; FC_ATCS_8k'; FO_ATCS1_8k'; FO_ATCS2_8k';FO_ATCS3_8k'; FO_ATCS4_8k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2', 'FCT-S:3', 'FCT-S:4'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(2),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_ATCS_8k), mean(VB_OD_ATCS_8k), mean(FB_OD_ATCS_8k), mean(FC_ATCS_8k), mean(FO_ATCS1_8k), mean(FO_ATCS2_8k), mean(FO_ATCS3_8k), mean(FO_ATCS4_8k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-.";":";"--"]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j = 1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j = 1:length(h)
    set(h(j),'Visible','off');
end

%% data of aggregate signal feature coverage at 16k seconds for 30 runs

ax(3)=subplot(1,3,3);

group = [    ones(size(ART_ATCS_16k'));
         2 * ones(size(VB_OD_ATCS_16k'));
         3 * ones(size(FB_OD_ATCS_16k'));
         4 * ones(size(FC_ATCS_16k'));
         5 * ones(size(FO_ATCS1_16k'));
         6 * ones(size(FO_ATCS2_16k'));
         7 * ones(size(FO_ATCS3_16k'));
         8 * ones(size(FO_ATCS4_16k'))];


bh1 = boxplot([ART_ATCS_16k'; VB_OD_ATCS_16k'; FB_OD_ATCS_16k'; FC_ATCS_16k'; FO_ATCS1_16k'; FO_ATCS2_16k';FO_ATCS3_16k'; FO_ATCS4_16k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2', 'FCT-S:3', 'FCT-S:4'});

xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box

set(bh1,'LineWidth',1.5);
axis(ax(3),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_ATCS_16k), mean(VB_OD_ATCS_16k), mean(FB_OD_ATCS_16k), mean(FC_ATCS_16k), mean(FO_ATCS1_16k), mean(FO_ATCS2_16k), mean(FO_ATCS3_16k), mean(FO_ATCS4_16k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
set(objs,'LineStyle',lst(j,1));
end

lst = ["-";"-";"-";"-";"--";"-.";":";"--"]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end

linkaxes(ax, 'y')

%% AECS model
load("AECS.mat");
figure;
clear bh1;
clear h;
clear objs;
clear color;
clear group;

clf()
ax = gobjects(3,1);

ax(1)=subplot(1,3,1);
color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70])];

group = [    ones(size(ART_AECS_1k'));
         2 * ones(size(VB_OD_AECS_1k'));
         3 * ones(size(FB_OD_AECS_1k'));
         4 * ones(size(FC_AECS_1k'));
         5 * ones(size(FO_AECS1_1k'))];


bh1 = boxplot([ART_AECS_1k'; VB_OD_AECS_1k'; FB_OD_AECS_1k'; FC_AECS_1k'; FO_AECS1_1k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(1),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_AECS_1k), mean(VB_OD_AECS_1k), mean(FB_OD_AECS_1k), mean(FC_AECS_1k), mean(FO_AECS1_1k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j =1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end


%% data of aggregate signal feature coverage at 8k seconds for 30 runs

ax(2)=subplot(1,3,2);
group = [    ones(size(ART_AECS_8k'));
         2 * ones(size(VB_OD_AECS_8k'));
         3 * ones(size(FB_OD_AECS_8k'));
         4 * ones(size(FC_AECS_8k'));
         5 * ones(size(FO_AECS1_8k'))];


bh1 = boxplot([ART_AECS_8k'; VB_OD_AECS_8k'; FB_OD_AECS_8k'; FC_AECS_8k'; FO_AECS1_8k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(2),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_AECS_8k), mean(VB_OD_AECS_8k), mean(FB_OD_AECS_8k), mean(FC_AECS_8k), mean(FO_AECS1_8k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j = 1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j = 1:length(h)
    set(h(j),'Visible','off');
end

%% data of aggregate signal feature coverage at 16k seconds for 30 runs

ax(3)=subplot(1,3,3);
group = [    ones(size(ART_AECS_16k'));
         2 * ones(size(VB_OD_AECS_16k'));
         3 * ones(size(FB_OD_AECS_16k'));
         4 * ones(size(FC_AECS_16k'));
         5 * ones(size(FO_AECS1_16k'))];


bh1 = boxplot([ART_AECS_16k'; VB_OD_AECS_16k'; FB_OD_AECS_16k'; FC_AECS_16k'; FO_AECS1_16k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});

xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box

set(bh1,'LineWidth',1.5);
axis(ax(3),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_AECS_16k), mean(VB_OD_AECS_16k), mean(FB_OD_AECS_16k), mean(FC_AECS_16k), mean(FO_AECS1_16k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
set(objs,'LineStyle',lst(j,1));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end

linkaxes(ax, 'y')

%% NN model
load("NN.mat");
figure;
clf()
ax = gobjects(3,1);

ax(1)=subplot(1,3,1);
color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55])];

group = [    ones(size(ART_NN_1k'));
         2 * ones(size(VB_OD_NN_1k'));
         3 * ones(size(FB_OD_NN_1k'));
         4 * ones(size(FC_NN_1k'));
         5 * ones(size(FO_NN1_1k'));
         6 * ones(size(FO_NN2_1k'))];


bh1 = boxplot([ART_NN_1k'; VB_OD_NN_1k'; FB_OD_NN_1k'; FC_NN_1k'; FO_NN1_1k'; FO_NN2_1k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(1),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_NN_1k), mean(VB_OD_NN_1k), mean(FB_OD_NN_1k), mean(FC_NN_1k), mean(FO_NN1_1k), mean(FO_NN2_1k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j =1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end


%% data of aggregate signal feature coverage at 8k seconds for 30 runs

ax(2)=subplot(1,3,2);

group = [    ones(size(ART_NN_8k'));
         2 * ones(size(VB_OD_NN_8k'));
         3 * ones(size(FB_OD_NN_8k'));
         4 * ones(size(FC_NN_8k'));
         5 * ones(size(FO_NN1_8k'));
         6 * ones(size(FO_NN2_8k'))];


bh1 = boxplot([ART_NN_8k'; VB_OD_NN_8k'; FB_OD_NN_8k'; FC_NN_8k'; FO_NN1_8k'; FO_NN2_8k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(2),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_NN_8k), mean(VB_OD_NN_8k), mean(FB_OD_NN_8k), mean(FC_NN_8k), mean(FO_NN1_8k), mean(FO_NN2_8k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j = 1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j = 1:length(h)
    set(h(j),'Visible','off');
end

%% data of aggregate signal feature coverage at 16k seconds for 30 runs

ax(3)=subplot(1,3,3);

group = [    ones(size(ART_NN_16k'));
         2 * ones(size(VB_OD_NN_16k'));
         3 * ones(size(FB_OD_NN_16k'));
         4 * ones(size(FC_NN_16k'));
         5 * ones(size(FO_NN1_16k'));
         6 * ones(size(FO_NN2_16k'))];


bh1 = boxplot([ART_NN_16k'; VB_OD_NN_16k'; FB_OD_NN_16k'; FC_NN_16k'; FO_NN1_16k'; FO_NN2_16k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});

xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box

set(bh1,'LineWidth',1.5);
axis(ax(3),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_NN_16k), mean(VB_OD_NN_16k), mean(FB_OD_NN_16k), mean(FC_NN_16k), mean(FO_NN1_16k), mean(FO_NN2_16k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
set(objs,'LineStyle',lst(j,1));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end

linkaxes(ax, 'y')

%% SC model
load("SC.mat");
figure;

clf()
ax = gobjects(3,1);

ax(1)=subplot(1,3,1);
color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70])];

group = [    ones(size(ART_SC_1k'));
         2 * ones(size(VB_OD_SC_1k'));
         3 * ones(size(FB_OD_SC_1k'));
         4 * ones(size(FC_SC_1k'));
         5 * ones(size(FO_SC1_1k'))];


bh1 = boxplot([ART_SC_1k'; VB_OD_SC_1k'; FB_OD_SC_1k'; FC_SC_1k'; FO_SC1_1k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(1),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_SC_1k), mean(VB_OD_SC_1k), mean(FB_OD_SC_1k), mean(FC_SC_1k), mean(FO_SC1_1k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j =1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end


%% data of aggregate signal feature coverage at 8k seconds for 30 runs

ax(2)=subplot(1,3,2);

group = [    ones(size(ART_SC_8k'));
         2 * ones(size(VB_OD_SC_8k'));
         3 * ones(size(FB_OD_SC_8k'));
         4 * ones(size(FC_SC_8k'));
         5 * ones(size(FO_SC1_8k'))];


bh1 = boxplot([ART_SC_8k'; VB_OD_SC_8k'; FB_OD_SC_8k'; FC_SC_8k'; FO_SC1_8k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});
xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box
set(bh1,'LineWidth',1.5);
axis(ax(2),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_SC_8k), mean(VB_OD_SC_8k), mean(FB_OD_SC_8k), mean(FC_SC_8k), mean(FO_SC1_8k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j = 1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j = 1:length(h)
    set(h(j),'Visible','off');
end

%% data of aggregate signal feature coverage at 16k seconds for 30 runs

ax(3)=subplot(1,3,3);

group = [    ones(size(ART_SC_16k'));
         2 * ones(size(VB_OD_SC_16k'));
         3 * ones(size(FB_OD_SC_16k'));
         4 * ones(size(FC_SC_16k'));
         5 * ones(size(FO_SC1_16k'))];


bh1 = boxplot([ART_SC_16k'; VB_OD_SC_16k'; FB_OD_SC_16k'; FC_SC_16k'; FO_SC1_16k'],group','Colors',color); %, 'PlotStyle','compact');
set(gca,'XTickLabel',{'ART','OD-VB', 'OD-FB','FCT-C', 'FCT-S:1', 'FCT-S:2'});

xtickangle(45)
set(gca,'fontsize',14, 'FontWeight', 'bold');
set(gca,'linewidth',1);% width of box

set(bh1,'LineWidth',1.5);
axis(ax(3),'tight')

set(gca,'fontname','Times New Roman');
hold on
plot([mean(ART_SC_16k), mean(VB_OD_SC_16k), mean(FB_OD_SC_16k), mean(FC_SC_16k), mean(FO_SC1_16k)],'k*')
hold off

color1 = flip(color);
h = findobj(gca,'Tag','Box'); 
for j = 1:length(h) 
objs = patch(get(h(j),'XData'),get(h(j),'YData'),get(h(j),'Color'),'FaceAlpha',.5);
set(objs,'FaceColor',color1(j,:), 'EdgeColor', color1(j,:));
set(objs,'LineStyle',lst(j,1));
end

lst = ["-";"-";"-";"-";"--";"-."]; % line style-- solid ART, solid VB-OD, solid FB-OD, solid FC, dash-dash FO1, dash-dot FO2
lst = flip(lst);
hline = findobj(gca,'type', 'line','Tag','Box'); 
for j = 1:length(hline) 
set(hline(j),'LineStyle',lst(j,1));
end

h = findobj(gca,'tag','Outliers');
for j=1:length(h) 
    set(h(j),'Color',color1(j,:));
    set(h(j),'MarkerEdgeColor', color1(j,:))
    set(h(j),'MarkerFaceColor', color1(j,:))
end

h = findobj(gca,'type', 'line','Tag','Median');
for j=1:length(h)
    set(h(j),'Visible','off');
end

linkaxes(ax, 'y')
