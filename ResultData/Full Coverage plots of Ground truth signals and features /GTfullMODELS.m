%% Tubular visulaiztaion of trends in signal feature coverage for ground truth features/signals achieved by various techniques
%  We show min, mean and max coverage

figure;

%% Load result data for all models and all properties
load('gtfulltubeFCS1.mat');
load('gtfulltubeFCS2.mat');
load('gtfulltubeATCS1.mat');
load('gtfulltubeATCS2.mat');
load('gtfulltubeATCS3.mat');
load('gtfulltubeATCS4.mat');
load('gtfulltubeAECS1.mat');
load('gtfulltubeNN1.mat');
load('gtfulltubeNN2.mat');
load('gtfulltubeSC1.mat');

colormap('default');


%% FCS
%% PROPERTY 1 - FCS

plot(time_budgetFCS,Min_ART_CovFCS_property1_GT, 'Color', Color(1,:));
hold on
plot(time_budgetFCS,Max_ART_CovFCS_property1_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_ART_CovFCS_property1_GT fliplr(Max_ART_CovFCS_property1_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART = plot(time_budgetFCS,Mean_ART_CovFCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_OVVB_CovFCS_property1_GT, 'Color', Color(2,:));
hold on
plot(time_budgetFCS,Max_OVVB_CovFCS_property1_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_OVVB_CovFCS_property1_GT fliplr(Max_OVVB_CovFCS_property1_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB = plot(time_budgetFCS,Mean_OVVB_CovFCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_OVFB_CovFCS_property1_GT, 'Color', Color(3,:));
hold on
plot(time_budgetFCS,Max_OVFB_CovFCS_property1_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_OVFB_CovFCS_property1_GT fliplr(Max_OVFB_CovFCS_property1_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB = plot(time_budgetFCS,Mean_OVFB_CovFCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_FC_CovFCS_property1_GT, 'Color', Color(4,:));
hold on
plot(time_budgetFCS,Max_FC_CovFCS_property1_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_FC_CovFCS_property1_GT fliplr(Max_FC_CovFCS_property1_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC =plot(time_budgetFCS,Mean_FC_CovFCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_FO_CovFCS_property1_GT, 'Color', Color(5,:));
hold on
plot(time_budgetFCS,Max_FO_CovFCS_property1_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_FO_CovFCS_property1_GT fliplr(Max_FO_CovFCS_property1_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO = plot(time_budgetFCS,Mean_FO_CovFCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{FCS:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');


%% PROPERTY 2 - FCS

figure;
plot(time_budgetFCS,Min_ART_CovFCS_property2_GT, 'Color', Color(1,:));
hold on
plot(time_budgetFCS,Max_ART_CovFCS_property2_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_ART_CovFCS_property2_GT fliplr(Max_ART_CovFCS_property2_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART =plot(time_budgetFCS,Mean_ART_CovFCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_OVVB_CovFCS_property2_GT, 'Color', Color(2,:));
hold on
plot(time_budgetFCS,Max_OVVB_CovFCS_property2_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_OVVB_CovFCS_property2_GT fliplr(Max_OVVB_CovFCS_property2_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB = plot(time_budgetFCS,Mean_OVVB_CovFCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_OVFB_CovFCS_property2_GT, 'Color', Color(3,:));
hold on
plot(time_budgetFCS,Max_OVFB_CovFCS_property2_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_OVFB_CovFCS_property2_GT fliplr(Max_OVFB_CovFCS_property2_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB = plot(time_budgetFCS,Mean_OVFB_CovFCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_FC_CovFCS_property2_GT, 'Color', Color(4,:));
hold on
plot(time_budgetFCS,Max_FC_CovFCS_property2_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_FC_CovFCS_property2_GT fliplr(Max_FC_CovFCS_property2_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC =plot(time_budgetFCS,Mean_FC_CovFCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_FO_CovFCS_property2_GT, 'Color', Color(5,:));
hold on
plot(time_budgetFCS,Max_FO_CovFCS_property2_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_FO_CovFCS_property2_GT fliplr(Max_FO_CovFCS_property2_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO = plot(time_budgetFCS,Mean_FO_CovFCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{FCS:}$ \boldmath{${\phi_2}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');

%% ATCS
%% PROPERTY 1 - ATCS

figure;
plot(time_budgetATCS,Min_ART_CovATCS_property1_GT, 'Color', Color(1,:));
hold on
plot(time_budgetATCS,Max_ART_CovATCS_property1_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_ART_CovATCS_property1_GT fliplr(Max_ART_CovATCS_property1_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART = plot(time_budgetATCS,Mean_ART_CovATCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_OVVB_CovATCS_property1_GT, 'Color', Color(2,:));
hold on
plot(time_budgetATCS,Max_OVVB_CovATCS_property1_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVVB_CovATCS_property1_GT fliplr(Max_OVVB_CovATCS_property1_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB =plot(time_budgetATCS,Mean_OVVB_CovATCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_OVFB_CovATCS_property1_GT, 'Color', Color(3,:));
hold on
plot(time_budgetATCS,Max_OVFB_CovATCS_property1_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVFB_CovATCS_property1_GT fliplr(Max_OVFB_CovATCS_property1_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB = plot(time_budgetATCS,Mean_OVFB_CovATCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FC_CovATCS_property1_GT, 'Color', Color(4,:));
hold on
plot(time_budgetATCS,Max_FC_CovATCS_property1_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FC_CovATCS_property1_GT fliplr(Max_FC_CovATCS_property1_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC =plot(time_budgetATCS,Mean_FC_CovATCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FO_CovATCS_property1_GT, 'Color', Color(5,:));
hold on
plot(time_budgetATCS,Max_FO_CovATCS_property1_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FO_CovATCS_property1_GT fliplr(Max_FO_CovATCS_property1_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO =plot(time_budgetATCS,Mean_FO_CovATCS_property1_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{ATCS:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');

%% PROPERTY 2 - ATCS

figure;
plot(time_budgetATCS,Min_ART_CovATCS_property2_GT, 'Color', Color(1,:));
hold on
plot(time_budgetATCS,Max_ART_CovATCS_property2_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_ART_CovATCS_property2_GT fliplr(Max_ART_CovATCS_property2_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART =plot(time_budgetATCS,Mean_ART_CovATCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_OVVB_CovATCS_property2_GT, 'Color', Color(2,:));
hold on
plot(time_budgetATCS,Max_OVVB_CovATCS_property2_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVVB_CovATCS_property2_GT fliplr(Max_OVVB_CovATCS_property2_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB = plot(time_budgetATCS,Mean_OVVB_CovATCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_OVFB_CovATCS_property2_GT, 'Color', Color(3,:));
hold on
plot(time_budgetATCS,Max_OVFB_CovATCS_property2_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVFB_CovATCS_property2_GT fliplr(Max_OVFB_CovATCS_property2_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB =plot(time_budgetATCS,Mean_OVFB_CovATCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FC_CovATCS_property2_GT, 'Color', Color(4,:));
hold on
plot(time_budgetATCS,Max_FC_CovATCS_property2_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FC_CovATCS_property2_GT fliplr(Max_FC_CovATCS_property2_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC =plot(time_budgetATCS,Mean_FC_CovATCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FO_CovATCS_property2_GT, 'Color', Color(5,:));
hold on
plot(time_budgetATCS,Max_FO_CovATCS_property2_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FO_CovATCS_property2_GT fliplr(Max_FO_CovATCS_property2_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO =plot(time_budgetATCS,Mean_FO_CovATCS_property2_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{ATCS:}$ \boldmath{${\phi_2}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');

%% PROPERTY 3 - ATCS

figure;
plot(time_budgetATCS,Min_ART_CovATCS_property3_GT, 'Color', Color(1,:));
hold on
plot(time_budgetATCS,Max_ART_CovATCS_property3_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_ART_CovATCS_property3_GT fliplr(Max_ART_CovATCS_property3_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART = plot(time_budgetATCS,Mean_ART_CovATCS_property3_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_OVVB_CovATCS_property3_GT, 'Color', Color(2,:));
hold on
plot(time_budgetATCS,Max_OVVB_CovATCS_property3_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVVB_CovATCS_property3_GT fliplr(Max_OVVB_CovATCS_property3_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB =plot(time_budgetATCS,Mean_OVVB_CovATCS_property3_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_OVFB_CovATCS_property3_GT, 'Color', Color(3,:));
hold on
plot(time_budgetATCS,Max_OVFB_CovATCS_property3_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVFB_CovATCS_property3_GT fliplr(Max_OVFB_CovATCS_property3_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB =plot(time_budgetATCS,Mean_OVFB_CovATCS_property3_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FC_CovATCS_property3_GT, 'Color', Color(4,:));
hold on
plot(time_budgetATCS,Max_FC_CovATCS_property3_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FC_CovATCS_property3_GT fliplr(Max_FC_CovATCS_property3_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC =plot(time_budgetATCS,Mean_FC_CovATCS_property3_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FO_CovATCS_property3_GT, 'Color', Color(5,:));
hold on
plot(time_budgetATCS,Max_FO_CovATCS_property3_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FO_CovATCS_property3_GT fliplr(Max_FO_CovATCS_property3_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO=plot(time_budgetATCS,Mean_FO_CovATCS_property3_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{ATCS:}$ \boldmath{${\phi_3}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');

%% PROPERTY 4 - ATCS

figure;
plot(time_budgetATCS,Min_ART_CovATCS_property4_GT, 'Color', Color(1,:));
hold on
plot(time_budgetATCS,Max_ART_CovATCS_property4_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_ART_CovATCS_property4_GT fliplr(Max_ART_CovATCS_property4_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART =plot(time_budgetATCS,Mean_ART_CovATCS_property4_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_OVVB_CovATCS_property4_GT, 'Color', Color(2,:));
hold on
plot(time_budgetATCS,Max_OVVB_CovATCS_property4_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVVB_CovATCS_property4_GT fliplr(Max_OVVB_CovATCS_property4_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB =plot(time_budgetATCS,Mean_OVVB_CovATCS_property4_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_OVFB_CovATCS_property4_GT, 'Color', Color(3,:));
hold on
plot(time_budgetATCS,Max_OVFB_CovATCS_property4_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVFB_CovATCS_property4_GT fliplr(Max_OVFB_CovATCS_property4_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB =plot(time_budgetATCS,Mean_OVFB_CovATCS_property4_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FC_CovATCS_property4_GT, 'Color', Color(4,:));
hold on
plot(time_budgetATCS,Max_FC_CovATCS_property4_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FC_CovATCS_property4_GT fliplr(Max_FC_CovATCS_property4_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC =plot(time_budgetATCS,Mean_FC_CovATCS_property4_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FO_CovATCS_property4_GT, 'Color', Color(5,:));
hold on
plot(time_budgetATCS,Max_FO_CovATCS_property4_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FO_CovATCS_property4_GT fliplr(Max_FO_CovATCS_property4_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO=plot(time_budgetATCS,Mean_FO_CovATCS_property4_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{ATCS:}$ \boldmath{${\phi_4}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');


%% AECS
%% PROPERTY 1 - AECS

figure;
plot(time_budgetAECS,Min_ART_CovAECS_property1_GT, 'Color', Color(1,:));
hold on
plot(time_budgetAECS,Max_ART_CovAECS_property1_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_ART_CovAECS_property1_GT fliplr(Max_ART_CovAECS_property1_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART = plot(time_budgetAECS,Mean_ART_CovAECS_property1_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetAECS,Min_OVVB_CovAECS_property1_GT, 'Color', Color(2,:));
hold on
plot(time_budgetAECS,Max_OVVB_CovAECS_property1_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_OVVB_CovAECS_property1_GT fliplr(Max_OVVB_CovAECS_property1_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB =plot(time_budgetAECS,Mean_OVVB_CovAECS_property1_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetAECS,Min_OVFB_CovAECS_property1_GT, 'Color', Color(3,:));
hold on
plot(time_budgetAECS,Max_OVFB_CovAECS_property1_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_OVFB_CovAECS_property1_GT fliplr(Max_OVFB_CovAECS_property1_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB =plot(time_budgetAECS,Mean_OVFB_CovAECS_property1_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetAECS,Min_FC_CovAECS_property1_GT, 'Color', Color(4,:));
hold on
plot(time_budgetAECS,Max_FC_CovAECS_property1_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_FC_CovAECS_property1_GT fliplr(Max_FC_CovAECS_property1_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC=plot(time_budgetAECS,Mean_FC_CovAECS_property1_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetAECS,Min_FO_CovAECS_property1_GT, 'Color', Color(5,:));
hold on
plot(time_budgetAECS,Max_FO_CovAECS_property1_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_FO_CovAECS_property1_GT fliplr(Max_FO_CovAECS_property1_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO =plot(time_budgetAECS,Mean_FO_CovAECS_property1_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{AECS:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');


%% NN
%% PROPERTY 1 - NN

figure;
plot(time_budgetNN,Min_ART_CovNN_property1_GT, 'Color', Color(1,:));
hold on
plot(time_budgetNN,Max_ART_CovNN_property1_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_ART_CovNN_property1_GT fliplr(Max_ART_CovNN_property1_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART = plot(time_budgetNN,Mean_ART_CovNN_property1_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_OVVB_CovNN_property1_GT, 'Color', Color(2,:));
hold on
plot(time_budgetNN,Max_OVVB_CovNN_property1_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_OVVB_CovNN_property1_GT fliplr(Max_OVVB_CovNN_property1_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB =plot(time_budgetNN,Mean_OVVB_CovNN_property1_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_OVFB_CovNN_property1_GT, 'Color', Color(3,:));
hold on
plot(time_budgetNN,Max_OVFB_CovNN_property1_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_OVFB_CovNN_property1_GT fliplr(Max_OVFB_CovNN_property1_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB =plot(time_budgetNN,Mean_OVFB_CovNN_property1_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_FC_CovNN_property1_GT, 'Color', Color(4,:));
hold on
plot(time_budgetNN,Max_FC_CovNN_property1_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_FC_CovNN_property1_GT fliplr(Max_FC_CovNN_property1_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC =plot(time_budgetNN,Mean_FC_CovNN_property1_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_FO_CovNN_property1_GT, 'Color', Color(5,:));
hold on
plot(time_budgetNN,Max_FO_CovNN_property1_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_FO_CovNN_property1_GT fliplr(Max_FO_CovNN_property1_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO =plot(time_budgetNN,Mean_FO_CovNN_property1_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{NN:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');

%% PROPERTY 2 - NN

figure;
plot(time_budgetNN,Min_ART_CovNN_property2_GT, 'Color', Color(1,:));
hold on
plot(time_budgetNN,Max_ART_CovNN_property2_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_ART_CovNN_property2_GT fliplr(Max_ART_CovNN_property2_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART =plot(time_budgetNN,Mean_ART_CovNN_property2_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_OVVB_CovNN_property2_GT, 'Color', Color(2,:));
hold on
plot(time_budgetNN,Max_OVVB_CovNN_property2_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_OVVB_CovNN_property2_GT fliplr(Max_OVVB_CovNN_property2_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB=plot(time_budgetNN,Mean_OVVB_CovNN_property2_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_OVFB_CovNN_property2_GT, 'Color', Color(3,:));
hold on
plot(time_budgetNN,Max_OVFB_CovNN_property2_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_OVFB_CovNN_property2_GT fliplr(Max_OVFB_CovNN_property2_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB =plot(time_budgetNN,Mean_OVFB_CovNN_property2_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_FC_CovNN_property2_GT, 'Color', Color(4,:));
hold on
plot(time_budgetNN,Max_FC_CovNN_property2_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_FC_CovNN_property2_GT fliplr(Max_FC_CovNN_property2_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC=plot(time_budgetNN,Mean_FC_CovNN_property2_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_FO_CovNN_property2_GT, 'Color', Color(5,:));
hold on
plot(time_budgetNN,Max_FO_CovNN_property2_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_FO_CovNN_property2_GT fliplr(Max_FO_CovNN_property2_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO=plot(time_budgetNN,Mean_FO_CovNN_property2_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{NN:}$ \boldmath{${\phi_2}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');


%% SC
%% PROPERTY 1 - SC

figure;
plot(time_budgetSC,Min_ART_CovSC_property1_GT, 'Color', Color(1,:));
hold on
plot(time_budgetSC,Max_ART_CovSC_property1_GT, 'Color', Color(1,:));
hold on
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_ART_CovSC_property1_GT fliplr(Max_ART_CovSC_property1_GT)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
ART = plot(time_budgetSC,Mean_ART_CovSC_property1_GT, 'LineWidth', 1.5, 'Color', Color(1,:));
alpha(0.2)
hold on;

plot(time_budgetSC,Min_OVVB_CovSC_property1_GT, 'Color', Color(2,:));
hold on
plot(time_budgetSC,Max_OVVB_CovSC_property1_GT, 'Color', Color(2,:));
hold on
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_OVVB_CovSC_property1_GT fliplr(Max_OVVB_CovSC_property1_GT)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
ODVB = plot(time_budgetSC,Mean_OVVB_CovSC_property1_GT, 'LineWidth', 1.5, 'Color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetSC,Min_OVFB_CovSC_property1_GT, 'Color', Color(3,:));
hold on
plot(time_budgetSC,Max_OVFB_CovSC_property1_GT, 'Color', Color(3,:));
hold on
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_OVFB_CovSC_property1_GT fliplr(Max_OVFB_CovSC_property1_GT)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
ODFB = plot(time_budgetSC,Mean_OVFB_CovSC_property1_GT, 'LineWidth', 1.5, 'Color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetSC,Min_FC_CovSC_property1_GT, 'Color', Color(4,:));
hold on
plot(time_budgetSC,Max_FC_CovSC_property1_GT, 'Color', Color(4,:));
hold on
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_FC_CovSC_property1_GT fliplr(Max_FC_CovSC_property1_GT)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
FC = plot(time_budgetSC,Mean_FC_CovSC_property1_GT, 'LineWidth', 1.5, 'Color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetSC,Min_FO_CovSC_property1_GT, 'Color', Color(5,:));
hold on
plot(time_budgetSC,Max_FO_CovSC_property1_GT, 'Color', Color(5,:));
hold on
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_FO_CovSC_property1_GT fliplr(Max_FO_CovSC_property1_GT)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
FO = plot(time_budgetSC,Mean_FO_CovSC_property1_GT, 'LineWidth', 1.5, 'Color', Color(5,:),'LineStyle', "-.");

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{SC:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);
legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');
