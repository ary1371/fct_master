%% trends in signal feature coverage for ground truth features achieved by various techniques
% we show mean value

figure;
colormap('default');

%% Load results for all models and properties
load('color.mat');
load('meanGTFCS.mat');
load('meanGTATCS.mat');
load('meanGTAECS.mat');
load('meanGTNN.mat');
load('meanGTSC.mat');

%% FCS
%% PROPERTY 1 - FCS
subplot(2,5,1);
plot(time_budgetFCS,Mean_ART_CovFCS_property1_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
plot(time_budgetFCS,Mean_OVVB_CovFCS_property1_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
plot(time_budgetFCS,Mean_OVFB_CovFCS_property1_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
plot(time_budgetFCS,Mean_FC_CovFCS_property1_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
plot(time_budgetFCS,Mean_FO_CovFCS_property1_GT, 'LineWidth', 2.5, 'Color', Color(5,:),'LineStyle', "-.");
set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{FCS:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);

%% PROPERTY 2 - FCS

subplot(2,5,2);
plot(time_budgetFCS,Mean_ART_CovFCS_property2_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
plot(time_budgetFCS,Mean_OVVB_CovFCS_property2_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
plot(time_budgetFCS,Mean_OVFB_CovFCS_property2_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
plot(time_budgetFCS,Mean_FC_CovFCS_property2_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
plot(time_budgetFCS,Mean_FO_CovFCS_property2_GT, 'LineWidth', 2.5, 'Color', Color(5,:),'LineStyle', "-.");
set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{FCS:}$ \boldmath{${\phi_2}$}'},'Interpreter','Latex', 'FontSize', 15);


%% ATCS
%% PROPERTY 1 - ATCS

subplot(2,5,3);
plot(time_budgetATCS,Mean_ART_CovATCS_property1_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
plot(time_budgetATCS,Mean_OVVB_CovATCS_property1_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
plot(time_budgetATCS,Mean_OVFB_CovATCS_property1_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
plot(time_budgetATCS,Mean_FC_CovATCS_property1_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
plot(time_budgetATCS,Mean_FO_CovATCS_property1_GT, '-.','LineWidth', 2.5, 'Color', Color(5,:));

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{ATCS:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);

%% PROPERTY 2 - ATCS

subplot(2,5,4);
plot(time_budgetATCS,Mean_ART_CovATCS_property2_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
plot(time_budgetATCS,Mean_OVVB_CovATCS_property2_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
plot(time_budgetATCS,Mean_OVFB_CovATCS_property2_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
plot(time_budgetATCS,Mean_FC_CovATCS_property2_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
plot(time_budgetATCS,Mean_FO_CovATCS_property2_GT, '-.','LineWidth', 2.5, 'Color', Color(5,:));

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{ATCS:}$ \boldmath{${\phi_2}$}'},'Interpreter','Latex', 'FontSize', 15);

%% PROPERTY 3 - ATCS

subplot(2,5,5);
plot(time_budgetATCS,Mean_ART_CovATCS_property3_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
plot(time_budgetATCS,Mean_OVVB_CovATCS_property3_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
plot(time_budgetATCS,Mean_OVFB_CovATCS_property3_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
plot(time_budgetATCS,Mean_FC_CovATCS_property3_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
plot(time_budgetATCS,Mean_FO_CovATCS_property3_GT, '-.','LineWidth', 2.5, 'Color', Color(5,:));

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{ATCS:}$ \boldmath{${\phi_3}$}'},'Interpreter','Latex', 'FontSize', 15);

%% PROPERTY 4 - ATCS

subplot(2,5,6);
plot(time_budgetATCS,Mean_ART_CovATCS_property4_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
plot(time_budgetATCS,Mean_OVVB_CovATCS_property4_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
plot(time_budgetATCS,Mean_OVFB_CovATCS_property4_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
plot(time_budgetATCS,Mean_FC_CovATCS_property4_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
plot(time_budgetATCS,Mean_FO_CovATCS_property4_GT, '-.','LineWidth', 2.5, 'Color', Color(5,:));

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{ATCS:}$ \boldmath{${\phi_4}$}'},'Interpreter','Latex', 'FontSize', 15);


%% AECS
%% PROPERTY 1 - AECS

subplot(2,5,7);
ART = plot(time_budgetAECS,Mean_ART_CovAECS_property1_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
ODVB = plot(time_budgetAECS,Mean_OVVB_CovAECS_property1_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
ODFB = plot(time_budgetAECS,Mean_OVFB_CovAECS_property1_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
FC = plot(time_budgetAECS,Mean_FC_CovAECS_property1_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
FO = plot(time_budgetAECS,Mean_FO_CovAECS_property1_GT, '-.','LineWidth', 2.5, 'Color', Color(5,:));

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{AECS:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);


%% NN
%% PROPERTY 1 - NN

subplot(2,5,8);
plot(time_budgetNN,Mean_ART_CovNN_property1_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
plot(time_budgetNN,Mean_OVVB_CovNN_property1_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
plot(time_budgetNN,Mean_OVFB_CovNN_property1_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
plot(time_budgetNN,Mean_FC_CovNN_property1_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
plot(time_budgetNN,Mean_FO_CovNN_property1_GT, '-.','LineWidth', 2.5, 'Color', Color(5,:));

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{NN:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);

%% PROPERTY 2 - NN

subplot(2,5,9);
plot(time_budgetNN,Mean_ART_CovNN_property2_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
plot(time_budgetNN,Mean_OVVB_CovNN_property2_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
plot(time_budgetNN,Mean_OVFB_CovNN_property2_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
plot(time_budgetNN,Mean_FC_CovNN_property2_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
plot(time_budgetNN,Mean_FO_CovNN_property2_GT, '-.','LineWidth', 2.5, 'Color', Color(5,:));

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{NN:}$ \boldmath{${\phi_2}$}'},'Interpreter','Latex', 'FontSize', 15);


%% SC
%% PROPERTY 1 - SC

subplot(2,5,10);
plot(time_budgetSC,Mean_ART_CovSC_property1_GT, 'LineWidth', 2.5, 'Color', Color(1,:));
hold on;
plot(time_budgetSC,Mean_OVVB_CovSC_property1_GT, 'LineWidth', 2.5, 'Color', Color(2,:));
hold on;
plot(time_budgetSC,Mean_OVFB_CovSC_property1_GT, 'LineWidth', 2.5, 'Color', Color(3,:));
hold on;
plot(time_budgetSC,Mean_FC_CovSC_property1_GT, 'LineWidth', 2.5, 'Color', Color(4,:));
hold on;
plot(time_budgetSC,Mean_FO_CovSC_property1_GT, '-.','LineWidth', 2.5, 'Color', Color(5,:));

set(gca,'fontsize', 14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])

xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{\phi}$}'},'Interpreter','Latex');
title({'$\mathbf{SC:}$ \boldmath{${\phi_1}$}'},'Interpreter','Latex', 'FontSize', 15);

%% Overall legend for the figure

leg = legend([ART ODVB ODFB FC FO],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S}'},'Interpreter','Latex','AutoUpdate','off', 'orientation','horizontal', 'Location','south');