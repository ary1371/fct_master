%% Tubular visualization: FULL COVERAGE trends over time for all models all properties
% We show min, max and mean values

%% Load Results data
load('MeanCovFCS.mat');
load('MeanCovATCS.mat');
load('MeanCovAECS.mat');
load('MeanCovNN.mat');
load('MeanCovSC.mat');
load('FullCovFCS.mat');
load('FullCovATCS.mat');
load('FullCovAECS.mat');
load('FullCovNN.mat');
load('FullCovSC.mat');

%% Plotting

%% Updated color
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55])];

%% Trends in aggregate signal feature coverage achieved by various techniques
%% FCS
figure;
colormap('default');

plot(time_budgetFCS,Min_ART_CovFCS, 'Color', Color(1,:));
hold on
plot(time_budgetFCS,Max_ART_CovFCS, 'Color', Color(1,:));
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_ART_CovFCS fliplr(Max_ART_CovFCS)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_ARTFCS = plot(time_budgetFCS,Mean_ART_CovFCS, 'LineWidth', 2, 'color', Color(1,:));
alpha(0.2)
hold on;


plot(time_budgetFCS,Min_OVVB_CovFCS, 'Color', Color(2,:));
hold on
plot(time_budgetFCS,Max_OVVB_CovFCS, 'Color', Color(2,:));
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_OVVB_CovFCS fliplr(Max_OVVB_CovFCS)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
h_OVVBFCS = plot(time_budgetFCS,Mean_OVVB_CovFCS, 'LineWidth', 2, 'color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_OVFB_CovFCS, 'Color', Color(3,:));
hold on
plot(time_budgetFCS,Max_OVFB_CovFCS, 'Color', Color(3,:));
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_OVFB_CovFCS fliplr(Max_OVFB_CovFCS)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
h_OVFBFCS = plot(time_budgetFCS,Mean_OVFB_CovFCS, 'LineWidth', 2, 'color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_FC_CovFCS, 'Color', Color(4,:));
hold on
plot(time_budgetFCS,Max_FC_CovFCS, 'Color', Color(4,:));
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_FC_CovFCS fliplr(Max_FC_CovFCS)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
h_FCFCS =plot(time_budgetFCS,Mean_FC_CovFCS, 'LineWidth', 2, 'color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetFCS,Min_FO_Cov_FCSproperty1, 'Color', Color(5,:));
hold on
plot(time_budgetFCS,Max_FO_Cov_FCSproperty1, 'Color', Color(5,:));
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_FO_Cov_FCSproperty1 fliplr(Max_FO_Cov_FCSproperty1)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_FO1FCS =plot(time_budgetFCS,Mean_FO_Cov_FCSproperty1, '--','LineWidth', 2, 'color', Color(5,:));
alpha(0.2)
hold on;


plot(time_budgetFCS,Min_FO_Cov_FCSproperty2, 'Color', Color(6,:));
hold on
plot(time_budgetFCS,Max_FO_Cov_FCSproperty2, 'Color', Color(6,:));
q = patch([time_budgetFCS fliplr(time_budgetFCS)], [Min_FO_Cov_FCSproperty2 fliplr(Max_FO_Cov_FCSproperty2)], Color(6,:));
q.LineWidth = 1;
q.EdgeColor = Color(6,:);
alpha(0.2)
h_FO2FCS =plot(time_budgetFCS,Mean_FO_Cov_FCSproperty2, '-.','LineWidth', 2, 'color', Color(6,:));
set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
leg = legend([h_ARTFCS h_OVVBFCS h_OVFBFCS h_FCFCS h_FO1FCS h_FO2FCS],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}','\textbf{FCT-S:} \boldmath{${\phi_2}$}'},'Interpreter','Latex','NumColumns',2,'AutoUpdate','off', 'orientation','horizontal', 'Location','south');
title({'$\mathbf{FCS}$'},'Interpreter','Latex', 'FontSize', 15);

%% ATCS Full tube plot

%% ATCS Model trends

%% Updated color
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.60 0.54 0.65])];

figure;
colormap('default');

plot(time_budgetATCS,Min_ART_CovATCS, 'Color', Color(1,:));
hold on
plot(time_budgetATCS,Max_ART_CovATCS, 'Color', Color(1,:));
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_ART_CovATCS fliplr(Max_ART_CovATCS)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_ARTATCS = plot(time_budgetATCS,Mean_ART_CovATCS, 'LineWidth', 2, 'color', Color(1,:));
alpha(0.2)
hold on;


plot(time_budgetATCS,Min_OVVB_CovATCS, 'Color', Color(2,:));
hold on
plot(time_budgetATCS,Max_OVVB_CovATCS, 'Color', Color(2,:));
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVVB_CovATCS fliplr(Max_OVVB_CovATCS)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
h_OVVBATCS = plot(time_budgetATCS,Mean_OVVB_CovATCS, 'LineWidth', 2, 'color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_OVFB_CovATCS, 'Color', Color(3,:));
hold on
plot(time_budgetATCS,Max_OVFB_CovATCS, 'Color', Color(3,:));
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_OVFB_CovATCS fliplr(Max_OVFB_CovATCS)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
h_OVFBATCS = plot(time_budgetATCS,Mean_OVFB_CovATCS, 'LineWidth', 2, 'color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FC_CovATCS, 'Color', Color(4,:));
hold on
plot(time_budgetATCS,Max_FC_CovATCS, 'Color', Color(4,:));
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FC_CovATCS fliplr(Max_FC_CovATCS)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
h_FCATCS =plot(time_budgetATCS,Mean_FC_CovATCS, 'LineWidth', 2, 'color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FO_Cov_ATCSproperty1, 'Color', Color(5,:));
hold on
plot(time_budgetATCS,Max_FO_Cov_ATCSproperty1, 'Color', Color(5,:));
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FO_Cov_ATCSproperty1 fliplr(Max_FO_Cov_ATCSproperty1)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_FO1ATCS =plot(time_budgetATCS,Mean_FO_Cov_propertyATCS1, '--','LineWidth', 2, 'color', Color(5,:));
alpha(0.2)
hold on;


plot(time_budgetATCS,Min_FO_Cov_ATCSproperty2, 'Color', Color(6,:));
hold on
plot(time_budgetATCS,Max_FO_Cov_ATCSproperty2, 'Color', Color(6,:));
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FO_Cov_ATCSproperty2 fliplr(Max_FO_Cov_ATCSproperty2)], Color(6,:));
q.LineWidth = 1;
q.EdgeColor = Color(6,:);
alpha(0.2)
h_FO2ATCS =plot(time_budgetATCS,Mean_FO_Cov_propertyATCS2, '-.','LineWidth', 2, 'color', Color(6,:));
alpha(0.2)
hold on;


plot(time_budgetATCS,Min_FO_Cov_ATCSproperty3, 'Color', Color(7,:));
hold on
plot(time_budgetATCS,Max_FO_Cov_ATCSproperty3, 'Color', Color(7,:));
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FO_Cov_ATCSproperty3 fliplr(Max_FO_Cov_ATCSproperty3)], Color(7,:));
q.LineWidth = 1;
q.EdgeColor = Color(7,:);
alpha(0.2)
h_FO3ATCS =plot(time_budgetATCS,Mean_FO_Cov_propertyATCS3, '-.','LineWidth', 2, 'color', Color(7,:));
alpha(0.2)
hold on;

plot(time_budgetATCS,Min_FO_Cov_ATCSproperty4, 'Color', Color(8,:));
hold on
plot(time_budgetATCS,Max_FO_Cov_ATCSproperty4, 'Color', Color(8,:));
q = patch([time_budgetATCS fliplr(time_budgetATCS)], [Min_FO_Cov_ATCSproperty4 fliplr(Max_FO_Cov_ATCSproperty4)], Color(8,:));
q.LineWidth = 1;
q.EdgeColor = Color(8,:);
alpha(0.2)
h_FO4ATCS =plot(time_budgetATCS,Mean_FO_Cov_propertyATCS4, '-.','LineWidth', 2, 'color', Color(8,:));
alpha(0.2)
hold on;

set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
leg = legend([h_ARTATCS h_OVVBATCS h_OVFBATCS h_FCATCS h_FO1ATCS h_FO2ATCS h_FO3ATCS h_FO4ATCS],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}','\textbf{FCT-S:} \boldmath{${\phi_2}$}','\textbf{FCT-S:} \boldmath{${\phi_3}$}','\textbf{FCT-S:} \boldmath{${\phi_4}$}'},'Interpreter','Latex','NumColumns',2,'AutoUpdate','off', 'orientation','horizontal', 'Location','south');

title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);

%% AECS Full tube plot
%% AECS Model trends
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.60 0.54 0.65])];

%% Trends in aggregate signal feature coverage achieved by various techniques

figure;
colormap('default');

plot(time_budgetAECS,Min_ART_CovAECS, 'Color', Color(1,:));
hold on
plot(time_budgetAECS,Max_ART_CovAECS, 'Color', Color(1,:));
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_ART_CovAECS fliplr(Max_ART_CovAECS)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_ARTAECS = plot(time_budgetAECS,Mean_ART_CovAECS, 'LineWidth', 2, 'color', Color(1,:));
alpha(0.2)
hold on;


plot(time_budgetAECS,Min_OVVB_CovAECS, 'Color', Color(2,:));
hold on
plot(time_budgetAECS,Max_OVVB_CovAECS, 'Color', Color(2,:));
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_OVVB_CovAECS fliplr(Max_OVVB_CovAECS)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
h_OVVBAECS = plot(time_budgetAECS,Mean_OVVB_CovAECS, 'LineWidth', 2, 'color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetAECS,Min_OVFB_CovAECS, 'Color', Color(3,:));
hold on
plot(time_budgetAECS,Max_OVFB_CovAECS, 'Color', Color(3,:));
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_OVFB_CovAECS fliplr(Max_OVFB_CovAECS)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
h_OVFBAECS = plot(time_budgetAECS,Mean_OVFB_CovAECS, 'LineWidth', 2, 'color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetAECS,Min_FC_CovAECS, 'Color', Color(4,:));
hold on
plot(time_budgetAECS,Max_FC_CovAECS, 'Color', Color(4,:));
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_FC_CovAECS fliplr(Max_FC_CovAECS)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
h_FCAECS =plot(time_budgetAECS,Mean_FC_CovAECS, 'LineWidth', 2, 'color', Color(4,:));
alpha(0.2)
hold on;

Mean_FO_Cov_propertyAECS1 = Mean_FO_CovAECS_property1;

plot(time_budgetAECS,Min_FO_Cov_AECSproperty1, 'Color', Color(5,:));
hold on
plot(time_budgetAECS,Max_FO_Cov_AECSproperty1, 'Color', Color(5,:));
q = patch([time_budgetAECS fliplr(time_budgetAECS)], [Min_FO_Cov_AECSproperty1 fliplr(Max_FO_Cov_AECSproperty1)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_FO1AECS =plot(time_budgetAECS,Mean_FO_Cov_propertyAECS1, '--','LineWidth', 2, 'color', Color(5,:));
alpha(0.2)
hold on;

set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
leg = legend([h_ARTAECS h_OVVBAECS h_OVFBAECS h_FCAECS h_FO1AECS],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}'},'Interpreter','Latex','NumColumns',2,'AutoUpdate','off', 'orientation','horizontal', 'Location','south');
title({'$\mathbf{AECS}$'},'Interpreter','Latex', 'FontSize', 15);

%% NN Full tube plot
%% NN Model trends
%% Updated color
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.60 0.54 0.65])];

%% Trends in aggregate signal feature coverage achieved by various techniques

figure;
colormap('default');

plot(time_budgetNN,Min_ART_CovNN, 'Color', Color(1,:));
hold on
plot(time_budgetNN,Max_ART_CovNN, 'Color', Color(1,:));
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_ART_CovNN fliplr(Max_ART_CovNN)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_ARTNN = plot(time_budgetNN,Mean_ART_CovNN, 'LineWidth', 2, 'color', Color(1,:));
alpha(0.2)
hold on;


plot(time_budgetNN,Min_OVVB_CovNN, 'Color', Color(2,:));
hold on
plot(time_budgetNN,Max_OVVB_CovNN, 'Color', Color(2,:));
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_OVVB_CovNN fliplr(Max_OVVB_CovNN)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
h_OVVBNN = plot(time_budgetNN,Mean_OVVB_CovNN, 'LineWidth', 2, 'color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_OVFB_CovNN, 'Color', Color(3,:));
hold on
plot(time_budgetNN,Max_OVFB_CovNN, 'Color', Color(3,:));
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_OVFB_CovNN fliplr(Max_OVFB_CovNN)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
h_OVFBNN = plot(time_budgetNN,Mean_OVFB_CovNN, 'LineWidth', 2, 'color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_FC_CovNN, 'Color', Color(4,:));
hold on
plot(time_budgetNN,Max_FC_CovNN, 'Color', Color(4,:));
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_FC_CovNN fliplr(Max_FC_CovNN)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
h_FCNN =plot(time_budgetNN,Mean_FC_CovNN, 'LineWidth', 2, 'color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetNN,Min_FO_Cov_NNproperty1, 'Color', Color(5,:));
hold on
plot(time_budgetNN,Max_FO_Cov_NNproperty1, 'Color', Color(5,:));
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_FO_Cov_NNproperty1 fliplr(Max_FO_Cov_NNproperty1)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_FO1NN =plot(time_budgetNN,Mean_FO_CovNN_property1, '--','LineWidth', 2, 'color', Color(5,:));
alpha(0.2)
hold on;


plot(time_budgetNN,Min_FO_Cov_NNproperty2, 'Color', Color(6,:));
hold on
plot(time_budgetNN,Max_FO_Cov_NNproperty2, 'Color', Color(6,:));
q = patch([time_budgetNN fliplr(time_budgetNN)], [Min_FO_Cov_NNproperty2 fliplr(Max_FO_Cov_NNproperty2)], Color(6,:));
q.LineWidth = 1;
q.EdgeColor = Color(6,:);
alpha(0.2)
h_FO2NN =plot(time_budgetNN,Mean_FO_CovNN_property2, '-.','LineWidth', 2, 'color', Color(6,:));
alpha(0.2)
hold on;

set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
leg = legend([h_ARTNN h_OVVBNN h_OVFBNN h_FCNN h_FO1NN h_FO2NN],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}','\textbf{FCT-S:} \boldmath{${\phi_2}$}'},'Interpreter','Latex','NumColumns',2,'AutoUpdate','off', 'orientation','horizontal', 'Location','south');

title({'$\mathbf{NN}$'},'Interpreter','Latex', 'FontSize', 15);

%% SC Full tube plot
%% SC Model trends

%% Updated color
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.60 0.54 0.65])];

%% Trends in aggregate signal feature coverage achieved by various techniques

figure;
colormap('default');


plot(time_budgetSC,Min_ART_CovSC, 'Color', Color(1,:));
hold on
plot(time_budgetSC,Max_ART_CovSC, 'Color', Color(1,:));
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_ART_CovSC fliplr(Max_ART_CovSC)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_ARTSC = plot(time_budgetSC,Mean_ART_CovSC, 'LineWidth', 2, 'color', Color(1,:));
alpha(0.2)
hold on;


plot(time_budgetSC,Min_OVVB_CovSC, 'Color', Color(2,:));
hold on
plot(time_budgetSC,Max_OVVB_CovSC, 'Color', Color(2,:));
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_OVVB_CovSC fliplr(Max_OVVB_CovSC)], Color(2,:));
q.LineWidth = 1;
q.EdgeColor = Color(2,:);
alpha(0.2)
h_OVVBSC = plot(time_budgetSC,Mean_OVVB_CovSC, 'LineWidth', 2, 'color', Color(2,:));
alpha(0.2)
hold on;

plot(time_budgetSC,Min_OVFB_CovSC, 'Color', Color(3,:));
hold on
plot(time_budgetSC,Max_OVFB_CovSC, 'Color', Color(3,:));
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_OVFB_CovSC fliplr(Max_OVFB_CovSC)], Color(3,:));
q.LineWidth = 1;
q.EdgeColor = Color(3,:);
alpha(0.2)
h_OVFBSC = plot(time_budgetSC,Mean_OVFB_CovSC, 'LineWidth', 2, 'color', Color(3,:));
alpha(0.2)
hold on;

plot(time_budgetSC,Min_FC_CovSC, 'Color', Color(4,:));
hold on
plot(time_budgetSC,Max_FC_CovSC, 'Color', Color(4,:));
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_FC_CovSC fliplr(Max_FC_CovSC)], Color(4,:));
q.LineWidth = 1;
q.EdgeColor = Color(4,:);
alpha(0.2)
h_FCSC =plot(time_budgetSC,Mean_FC_CovSC, 'LineWidth', 2, 'color', Color(4,:));
alpha(0.2)
hold on;

plot(time_budgetSC,Min_FO_Cov_SCproperty1, 'Color', Color(5,:));
hold on
plot(time_budgetSC,Max_FO_Cov_SCproperty1, 'Color', Color(5,:));
q = patch([time_budgetSC fliplr(time_budgetSC)], [Min_FO_Cov_SCproperty1 fliplr(Max_FO_Cov_SCproperty1)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_FO1SC =plot(time_budgetSC,Mean_FO_CovSC_property1, '--','LineWidth', 2, 'color', Color(5,:));
alpha(0.2)
hold on;

set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
leg = legend([h_ARTSC h_OVVBSC h_OVFBSC h_FCSC h_FO1SC],{'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}'},'Interpreter','Latex','NumColumns',2,'AutoUpdate','off', 'orientation','horizontal', 'Location','south');

title({'$\mathbf{SC}$'},'Interpreter','Latex', 'FontSize', 15);


