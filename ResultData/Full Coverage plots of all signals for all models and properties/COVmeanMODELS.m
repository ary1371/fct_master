%% Mean COVERAGE trends over time for all models all properties

%% Load Results data
load('MeanCovFCS.mat');
load('MeanCovATCS.mat');
load('MeanCovAECS.mat');
load('MeanCovNN.mat');
load('MeanCovSC.mat');

%% Plotting

%% FCS Model trends

%% Updated color
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.1 0.4 0.6])];

%% Trends in aggregate signal feature coverage achieved by various techniques

%% FCS
figure;
colormap('default');
time_budgetFCS = [0.00 0.10 0.20 0.24 0.27 0.30 0.40 0.45 0.47 0.50 0.60 0.70 0.80 0.90 1.00 2.00 3.00 4.00 5.00 6.00 7.00 8.00 9.00 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0 19.0 20.0];

plot(time_budgetFCS,Mean_ART_CovFCS, 'LineWidth', 2, 'color', Color(1,:));
hold on;
plot(time_budgetFCS,Mean_OVVB_CovFCS, 'LineWidth', 2, 'color', Color(2,:));
hold on;
plot(time_budgetFCS,Mean_OVFB_CovFCS, 'LineWidth', 2, 'color', Color(3,:));
hold on;
plot(time_budgetFCS,Mean_FC_CovFCS, 'LineWidth', 2, 'color', Color(4,:));
hold on;
plot(time_budgetFCS,Mean_FO_Cov_FCSproperty1, '--','LineWidth', 2, 'color', Color(5,:));
hold on;
plot(time_budgetFCS,Mean_FO_Cov_FCSproperty2, '-.','LineWidth', 2, 'color', Color(6,:));
set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
legend({'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}','\textbf{FCT-S:} \boldmath{${\phi_2}$}'},'Interpreter','Latex','NumColumns',2);
title({'$\mathbf{FCS}$'},'Interpreter','Latex', 'FontSize', 15);


%% ATCS
%% ATCS Model trends

%% Updated color
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.60 0.54 0.65])];

%% Trends in aggregate signal feature coverage achieved by various techniques


%% ATCS
time_budgetATCS =           [0.00 0.10 0.20 0.24 0.27 0.30 0.35 0.37 0.38 0.40 0.50 0.60 0.70 0.80 0.90 1.00 2.00 3.00 4.00 5.00 6.00 7.00 8.00 9.00 10.0 11.0 12.0 13.0 14.0 15.0 16.0 17.0 18.0 19.0 20.0];
Mean_ART_CovATCS =          [1.21 1.23 1.25 1.27 1.27 1.27 1.27 1.27 1.28 1.28 1.29 1.30 1.31 1.32 1.33 1.35 1.36 1.37 1.38 1.40 1.42 1.45 1.47 1.53 1.63 1.67 1.73 1.82 1.94 1.98 2.01 2.03 2.03 2.04 2.04];
Mean_OVVB_CovATCS =         [1.18 1.19 1.20 1.20 1.21 1.22 1.23 1.24 1.25 1.25 1.26 1.27 1.28 1.29 1.30 1.32 1.33 1.34 1.35 1.37 1.38 1.40 1.42 1.44 1.47 1.49 1.58 1.64 1.73 1.85 1.98 1.99 2.01 2.01 2.01];
Mean_OVFB_CovATCS =         [1.19 1.20 1.21 1.22 1.22 1.23 1.23 1.24 1.24 1.25 1.25 1.27 1.28 1.29 1.31 1.32 1.34 1.35 1.36 1.37 1.39 1.41 1.44 1.46 1.49 1.51 1.60 1.65 1.74 1.89 1.99 2.01 2.02 2.02 2.02];
Mean_FC_CovATCS =           [1.21 1.22 1.23 1.24 1.26 1.27 1.58 1.62 1.73 1.74 1.75 1.76 1.77 1.84 1.99 2.16 2.22 2.34 2.46 2.68 2.83 2.95 3.09 3.32 3.47 3.72 3.98 4.12 4.38 4.47 4.56 4.62 4.62 4.63 4.63];
Mean_FO_Cov_propertyATCS1 = [0.00 0.00 0.00 1.21 1.22 1.23 1.25 1.27 1.45 1.46 1.47 1.48 1.49 1.50 1.51 1.53 1.64 1.73 1.85 1.96 2.04 2.15 2.23 2.36 2.45 2.52 2.68 2.83 2.97 3.01 3.19 3.28 3.35 3.47 3.48];
Mean_FO_Cov_propertyATCS2 = [0.00 0.00 0.00 1.21 1.22 1.23 1.24 1.27 1.38 1.39 1.40 1.41 1.42 1.43 1.44 1.46 1.54 1.67 1.74 1.86 1.98 2.09 2.17 2.25 2.38 2.47 2.59 2.71 2.89 2.99 3.11 3.23 3.29 3.38 3.39];
Mean_FO_Cov_propertyATCS3 = [0.00 0.00 0.00 1.21 1.22 1.23 1.24 1.26 1.29 1.34 1.37 1.38 1.39 1.41 1.43 1.52 1.64 1.71 1.83 1.94 2.01 2.11 2.19 2.27 2.38 2.49 2.68 2.75 2.89 2.98 3.12 3.34 3.37 3.37 3.38];
Mean_FO_Cov_propertyATCS4 = [0.00 0.00 0.00 1.21 1.22 1.25 1.27 1.29 1.33 1.36 1.37 1.39 1.52 1.53 1.54 1.78 1.96 2.18 2.34 2.67 2.78 2.89 2.97 3.18 3.37 3.63 3.78 3.97 4.21 4.27 4.38 4.39 4.39 4.39 4.41]; 

figure;
colormap('default');

plot(time_budgetATCS,Mean_ART_CovATCS, 'LineWidth', 2, 'color', Color(1,:));
hold on;
plot(time_budgetATCS,Mean_OVVB_CovATCS, 'LineWidth', 2, 'color', Color(2,:));
hold on;
plot(time_budgetATCS,Mean_OVFB_CovATCS, 'LineWidth', 2, 'color', Color(3,:));
hold on;
plot(time_budgetATCS,Mean_FC_CovATCS, 'LineWidth', 2, 'color', Color(4,:));
hold on;
plot(time_budgetATCS,Mean_FO_Cov_propertyATCS1, '--','LineWidth', 2, 'color', Color(5,:));
hold on;
plot(time_budgetATCS,Mean_FO_Cov_propertyATCS2, '-.','LineWidth', 2, 'color', Color(6,:));
hold on;
plot(time_budgetATCS,Mean_FO_Cov_propertyATCS3, ':','LineWidth', 2, 'color', Color(7,:));
hold on;
plot(time_budgetATCS,Mean_FO_Cov_propertyATCS4, '-','LineWidth', 2, 'color', Color(8,:));
set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box);
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
legend({'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}','\textbf{FCT-S:} \boldmath{${\phi_2}$}','\textbf{FCT-S:} \boldmath{${\phi_3}$}','\textbf{FCT-S:} \boldmath{${\phi_4}$}'},'Interpreter','Latex','NumColumns',2);
title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);

%% AECS Model trends

%% Updated color
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.60 0.54 0.65])];

%% Trends in aggregate signal feature coverage achieved by various techniques

figure;
colormap('default');

plot(time_budgetAECS,Mean_ART_CovAECS, 'LineWidth', 2, 'color', Color(1,:));
hold on;
plot(time_budgetAECS,Mean_OVVB_CovAECS, 'LineWidth', 2, 'color', Color(2,:));
hold on;
plot(time_budgetAECS,Mean_OVFB_CovAECS, 'LineWidth', 2, 'color', Color(3,:));
hold on;
plot(time_budgetAECS,Mean_FC_CovAECS, 'LineWidth', 2, 'color', Color(4,:));
hold on;
plot(time_budgetAECS,Mean_FO_CovAECS_property1, '--','LineWidth', 2, 'color', Color(5,:));
hold on;
set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box);
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
legend({'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}'},'Interpreter','Latex','NumColumns',2);
title({'$\mathbf{AECS}$'},'Interpreter','Latex', 'FontSize', 15);

%% NN Model trends

%% Updated color
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.1 0.4 0.6])];

%% Trends in aggregate signal feature coverage achieved by various techniques

%% NN
figure;
colormap('default');

plot(time_budgetNN,Mean_ART_CovNN, 'LineWidth', 2, 'color', Color(1,:));
hold on;
plot(time_budgetNN,Mean_OVVB_CovNN, 'LineWidth', 2, 'color', Color(2,:));
hold on;
plot(time_budgetNN,Mean_OVFB_CovNN, 'LineWidth', 2, 'color', Color(3,:));
hold on;
plot(time_budgetNN,Mean_FC_CovNN, 'LineWidth', 2, 'color', Color(4,:));
hold on;
plot(time_budgetNN,Mean_FO_CovNN_property1, '--','LineWidth', 2, 'color', Color(5,:));
hold on;
plot(time_budgetNN,Mean_FO_CovNN_property2, '-.','LineWidth', 2, 'color', Color(6,:));
set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
legend({'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}','\textbf{FCT-S:} \boldmath{${\phi_2}$}'},'Interpreter','Latex','NumColumns',2);
title({'$\mathbf{NN}$'},'Interpreter','Latex', 'FontSize', 15);

%% SC Model trends

%% Updated color
Color = [([1 0 0]); ([0 0 1]); ([0.47 0.67 0.19]);( [0 0 0]);([0.67 0.68 0.70]); ([0.57 0.55 0.55]); ([0.7 0.6 0.5]); ([0.60 0.54 0.65])];

%% Trends in aggregate signal feature coverage achieved by various techniques
figure;
colormap('default');

plot(time_budgetSC,Mean_ART_CovSC, 'LineWidth', 2, 'color', Color(1,:));
hold on;
plot(time_budgetSC,Mean_OVVB_CovSC, 'LineWidth', 2, 'color', Color(2,:));
hold on;
plot(time_budgetSC,Mean_OVFB_CovSC, 'LineWidth', 2, 'color', Color(3,:));
hold on;
plot(time_budgetSC,Mean_FC_CovSC, 'LineWidth', 2, 'color', Color(4,:));
hold on;
plot(time_budgetSC,Mean_FO_CovSC_property1, '--','LineWidth', 2, 'color', Color(5,:));
hold on;
set(gca,'fontsize',16, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box);
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time Budget ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\textbf{Mean} \boldmath{$\zeta_{comp}$}'},'Interpreter','Latex');
legend({'\textbf{ART}','\textbf{OD-VB}','\textbf{OD-FB}','\textbf{FCT-C}','\textbf{FCT-S:} \boldmath{${\phi_1}$}'},'Interpreter','Latex','NumColumns',2);
title({'$\mathbf{SC}$'},'Interpreter','Latex', 'FontSize', 15);




