%% Tubular visualization: trends of mutation scores w.r.t. time for different test generation strategies

% COLOR SEQUENCE: ART, FT, VB-OD, FB-OD, FC, FO

load('PBMTfulldata.mat');
load('PBMTfullFCS.mat');
load('PBMTfullATCS.mat');
load('PBMTfullAECS.mat');
load('PBMTfullNN.mat');
load('PBMTfullSC.mat');

figure;
subplot(2,5,1);

%% ART
plot(time_budget1, min_ARTFCS1, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTFCS1, 'Color', Color(1,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_ARTFCS1 fliplr(max_ARTFCS1)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_art = plot(time_budget1, mean_ARTFCS1,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTFCS1, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTFCS1, 'Color', Color(2,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_FTFCS1 fliplr(max_FTFCS1)], Color(2,:));
p.LineWidth = 1;
p.EdgeColor = Color(2,:);
alpha(0.2)
h_ft = plot(time_budget1, mean_FTFCS1,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODFCS1, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODFCS1, 'Color', Color(3,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_VBODFCS1 fliplr(max_VBODFCS1)], Color(3,:));
p.LineWidth = 1;
p.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod = plot(time_budget1, mean_VBODFCS1,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD
plot(time_budget1, min_FBODFCS1, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODFCS1, 'Color', Color(4,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_FBODFCS1 fliplr(max_FBODFCS1)], Color(4,:));
p.LineWidth = 1;
p.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod = plot(time_budget1, mean_FBODFCS1,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCFCS1, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCFCS1, 'Color', Color(5,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_FCFCS1 fliplr(max_FCFCS1)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_fc = plot(time_budget1, mean_FCFCS1,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FOFCS1, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FOFCS1, 'Color', Color(6,:), 'LineStyle', "-.");
q = patch([time_budget1 fliplr(time_budget1)], [min_FOFCS1 fliplr(max_FOFCS1)], Color(6,:), 'LineStyle', "-.");
q.LineWidth = 1;
q.EdgeColor = Color(6,:);
alpha(0.2)
h_fo = plot(time_budget1, mean_FOFCS1,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)

set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_1^{FCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');

title({'$\mathbf{FCS}$'},'Interpreter','Latex', 'FontSize', 15);


%% Plots for mutation testing w.r.t. property 2
subplot(2,5,2);

%% ART
plot(time_budget1, min_ARTFCS2, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTFCS2, 'Color', Color(1,:));
q1 = patch([time_budget1 fliplr(time_budget1)], [min_ARTFCS2 fliplr(max_ARTFCS2)], Color(1,:));
q1.LineWidth = 1;
q1.EdgeColor = Color(1,:);
alpha(0.2)
h_art1 = plot(time_budget1, mean_ARTFCS2,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTFCS2, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTFCS2, 'Color', Color(2,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FTFCS2 fliplr(max_FTFCS2)], Color(2,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(2,:);
alpha(0.2)
h_ft1 = plot(time_budget1, mean_FTFCS2,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODFCS2, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODFCS2, 'Color', Color(3,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_VBODFCS2 fliplr(max_VBODFCS2)], Color(3,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod1 = plot(time_budget1, mean_VBODFCS2,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD
plot(time_budget1, min_FBODFCS2, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODFCS2, 'Color', Color(4,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FBODFCS2 fliplr(max_FBODFCS2)], Color(4,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod1 = plot(time_budget1, mean_FBODFCS2,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCFCS2, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCFCS2, 'Color', Color(5,:));
q1 = patch([time_budget1 fliplr(time_budget1)], [min_FCFCS2 fliplr(max_FCFCS2)], Color(5,:));
q1.LineWidth = 1;
q1.EdgeColor = Color(5,:);
alpha(0.2)
h_fc1 = plot(time_budget1, mean_FCFCS2,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FOFCS2, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FOFCS2, 'Color', Color(6,:), 'LineStyle', "-.");
q1 = patch([time_budget1 fliplr(time_budget1)], [min_FOFCS2 fliplr(max_FOFCS2)], Color(6,:), 'LineStyle', "-.");
q1.LineWidth = 1;
q1.EdgeColor = Color(6,:);
alpha(0.2)
h_fo1 = plot(time_budget1, mean_FOFCS2,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)

set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_2^{FCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{FCS}$'},'Interpreter','Latex', 'FontSize', 15);

%% ATCS mutation testing
%% Plots for mutation testing w.r.t. property 1

subplot(2,5,3);

%% ART
plot(time_budget1, min_ARTATCS1, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTATCS1, 'Color', Color(1,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_ARTATCS1 fliplr(max_ARTATCS1)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_art = plot(time_budget1, mean_ARTATCS1,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTATCS1, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTATCS1, 'Color', Color(2,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_FTATCS1 fliplr(max_FTATCS1)], Color(2,:));
p.LineWidth = 1;
p.EdgeColor = Color(2,:);
alpha(0.2)
h_ft = plot(time_budget1, mean_FTATCS1,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODATCS1, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODATCS1, 'Color', Color(3,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_VBODATCS1 fliplr(max_VBODATCS1)], Color(3,:));
p.LineWidth = 1;
p.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod = plot(time_budget1, mean_VBODATCS1,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD
plot(time_budget1, min_FBODATCS1, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODATCS1, 'Color', Color(4,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FBODATCS1 fliplr(max_FBODATCS1)], Color(4,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod = plot(time_budget1, mean_FBODATCS1,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCATCS1, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCATCS1, 'Color', Color(5,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_FCATCS1 fliplr(max_FCATCS1)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_fc = plot(time_budget1, mean_FCATCS1,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FOATCS1, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FOATCS1, 'Color', Color(6,:), 'LineStyle', "-.");
q = patch([time_budget1 fliplr(time_budget1)], [min_FOATCS1 fliplr(max_FOATCS1)], Color(6,:), 'LineStyle', "-.");
q.LineWidth = 1;
q.EdgeColor = Color(6,:);
alpha(0.2)
h_fo = plot(time_budget1, mean_FOATCS1,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)

set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); %set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_1^{ATCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');

title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);%, 'HorizontalAlignment', 'left');


%% Plots for mutation testing w.r.t. property 2
subplot(2,5,4);

%% ART
plot(time_budget1, min_ARTATCS2, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTATCS2, 'Color', Color(1,:));
q1 = patch([time_budget1 fliplr(time_budget1)], [min_ARTATCS2 fliplr(max_ARTATCS2)], Color(1,:));
q1.LineWidth = 1;
q1.EdgeColor = Color(1,:);
alpha(0.2)
h_art1 = plot(time_budget1, mean_ARTATCS2,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTATCS2, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTATCS2, 'Color', Color(2,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FTATCS2 fliplr(max_FTATCS2)], Color(2,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(2,:);
alpha(0.2)
h_ft1 = plot(time_budget1, mean_FTATCS2,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODATCS2, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODATCS2, 'Color', Color(3,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_VBODATCS2 fliplr(max_VBODATCS2)], Color(3,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod1 = plot(time_budget1, mean_VBODATCS2,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD
plot(time_budget1, min_FBODATCS2, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODATCS2, 'Color', Color(4,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FBODATCS2 fliplr(max_FBODATCS2)], Color(4,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod1 = plot(time_budget1, mean_FBODATCS2,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCATCS2, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCATCS2, 'Color', Color(5,:));
q1 = patch([time_budget1 fliplr(time_budget1)], [min_FCATCS2 fliplr(max_FCATCS2)], Color(5,:));
q1.LineWidth = 1;
q1.EdgeColor = Color(5,:);
alpha(0.2)
h_fc1 = plot(time_budget1, mean_FCATCS2,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FOATCS2, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FOATCS2, 'Color', Color(6,:), 'LineStyle', "-.");
q1 = patch([time_budget1 fliplr(time_budget1)], [min_FOATCS2 fliplr(max_FOATCS2)], Color(6,:), 'LineStyle', "-.");
q1.LineWidth = 1;
q1.EdgeColor = Color(6,:);
alpha(0.2)
h_fo1 = plot(time_budget1, mean_FOATCS2,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)

set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_2^{ATCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');

title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);%, 'HorizontalAlignment', 'left');

%% Plots for mutation testing w.r.t. property 3

subplot(2,5,5);

%% ART
plot(time_budget1, min_ARTATCS3, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTATCS3, 'Color', Color(1,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_ARTATCS3 fliplr(max_ARTATCS3)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_art = plot(time_budget1, mean_ARTATCS3,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTATCS3, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTATCS3, 'Color', Color(2,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_FTATCS3 fliplr(max_FTATCS3)], Color(2,:));
p.LineWidth = 1;
p.EdgeColor = Color(2,:);
alpha(0.2)
h_ft = plot(time_budget1, mean_FTATCS3,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODATCS3, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODATCS3, 'Color', Color(3,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_VBODATCS3 fliplr(max_VBODATCS3)], Color(3,:));
p.LineWidth = 1;
p.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod = plot(time_budget1, mean_VBODATCS3,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD
plot(time_budget1, min_FBODATCS3, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODATCS3, 'Color', Color(4,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FBODATCS3 fliplr(max_FBODATCS3)], Color(4,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod = plot(time_budget1, mean_FBODATCS3,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCATCS3, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCATCS3, 'Color', Color(5,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_FCATCS3 fliplr(max_FCATCS3)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_fc = plot(time_budget1, mean_FCATCS3,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FOATCS3, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FOATCS3, 'Color', Color(6,:), 'LineStyle', "-.");
q = patch([time_budget1 fliplr(time_budget1)], [min_FOATCS3 fliplr(max_FOATCS3)], Color(6,:), 'LineStyle', "-.");
q.LineWidth = 1;
q.EdgeColor = Color(6,:);
alpha(0.2)
h_fo = plot(time_budget1, mean_FOATCS3,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)

set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_3^{ATCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');

title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);%, 'HorizontalAlignment', 'left');


%% Plots for mutation testing w.r.t. property 4
subplot(2,5,6);

%% ART
plot(time_budget1, min_ARTATCS4, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTATCS4, 'Color', Color(1,:));
q1 = patch([time_budget1 fliplr(time_budget1)], [min_ARTATCS4 fliplr(max_ARTATCS4)], Color(1,:));
q1.LineWidth = 1;
q1.EdgeColor = Color(1,:);
alpha(0.2)
h_art1 = plot(time_budget1, mean_ARTATCS4,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTATCS4, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTATCS4, 'Color', Color(2,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FTATCS4 fliplr(max_FTATCS4)], Color(2,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(2,:);
alpha(0.2)
h_ft1 = plot(time_budget1, mean_FTATCS4,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODATCS4, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODATCS4, 'Color', Color(3,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_VBODATCS4 fliplr(max_VBODATCS4)], Color(3,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod1 = plot(time_budget1, mean_VBODATCS4,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD
plot(time_budget1, min_FBODATCS4, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODATCS4, 'Color', Color(4,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FBODATCS4 fliplr(max_FBODATCS4)], Color(4,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod1 = plot(time_budget1, mean_FBODATCS4,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCATCS4, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCATCS4, 'Color', Color(5,:));
q1 = patch([time_budget1 fliplr(time_budget1)], [min_FCATCS4 fliplr(max_FCATCS4)], Color(5,:));
q1.LineWidth = 1;
q1.EdgeColor = Color(5,:);
alpha(0.2)
h_fc1 = plot(time_budget1, mean_FCATCS4,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FOATCS4, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FOATCS4, 'Color', Color(6,:), 'LineStyle', "-.");
q1 = patch([time_budget1 fliplr(time_budget1)], [min_FOATCS4 fliplr(max_FOATCS4)], Color(6,:), 'LineStyle', "-.");
q1.LineWidth = 1;
q1.EdgeColor = Color(6,:);
alpha(0.2)
h_fo1 = plot(time_budget1, mean_FOATCS4,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)

set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_4^{ATCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');

title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);


%% 
%% AECS MODEL--for mutation testing w.r.t. property 1

subplot(2,5,7);
%% ART
plot(time_budget1, min_ARTAECS, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTAECS, 'Color', Color(1,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_ARTAECS fliplr(max_ARTAECS)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_art = plot(time_budget1, mean_ARTAECS,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTAECS, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTAECS, 'Color', Color(2,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_FTAECS fliplr(max_FTAECS)], Color(2,:));
p.LineWidth = 1;
p.EdgeColor = Color(2,:);
alpha(0.2)
h_ft = plot(time_budget1, mean_FTAECS,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODAECS, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODAECS, 'Color', Color(3,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_VBODAECS fliplr(max_VBODAECS)], Color(3,:));
p.LineWidth = 1;
p.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod = plot(time_budget1, mean_VBODAECS,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD
plot(time_budget1, min_FBODAECS, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODAECS, 'Color', Color(4,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FBODAECS fliplr(max_FBODAECS)], Color(4,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod = plot(time_budget1, mean_FBODAECS,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCAECS, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCAECS, 'Color', Color(5,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_FCAECS fliplr(max_FCAECS)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_fc = plot(time_budget1, mean_FCAECS,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FOAECS, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FOAECS, 'Color', Color(6,:), 'LineStyle', "-.");
q = patch([time_budget1 fliplr(time_budget1)], [min_FOAECS fliplr(max_FOAECS)], Color(6,:), 'LineStyle', "-.");
q.LineWidth = 1;
q.EdgeColor = Color(6,:);
alpha(0.2)
h_fo = plot(time_budget1, mean_FOAECS,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)

set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_1^{AECS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');

title({'$\mathbf{AECS}$'},'Interpreter','Latex', 'FontSize', 15);%, 'HorizontalAlignment', 'left');

%%
%% NN property 1

subplot(2,5,8);

%% ART
plot(time_budget1, min_ARTNN1, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTNN1, 'Color', Color(1,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_ARTNN1 fliplr(max_ARTNN1)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_art = plot(time_budget1, mean_ARTNN1,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTNN1, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTNN1, 'Color', Color(2,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_FTNN1 fliplr(max_FTNN1)], Color(2,:));
p.LineWidth = 1;
p.EdgeColor = Color(2,:);
alpha(0.2)
h_ft = plot(time_budget1, mean_FTNN1,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODNN1, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODNN1, 'Color', Color(3,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_VBODNN1 fliplr(max_VBODNN1)], Color(3,:));
p.LineWidth = 1;
p.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod = plot(time_budget1, mean_VBODNN1,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD
plot(time_budget1, min_FBODNN1, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODNN1, 'Color', Color(4,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FBODNN1 fliplr(max_FBODNN1)], Color(4,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod = plot(time_budget1, mean_FBODNN1,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCNN1, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCNN1, 'Color', Color(5,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_FCNN1 fliplr(max_FCNN1)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_fc = plot(time_budget1, mean_FCNN1,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FONN1, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FONN1, 'Color', Color(6,:), 'LineStyle', "-.");
q = patch([time_budget1 fliplr(time_budget1)], [min_FONN1 fliplr(max_FONN1)], Color(6,:), 'LineStyle', "-.");
q.LineWidth = 1;
q.EdgeColor = Color(6,:);
alpha(0.2)
h_fo = plot(time_budget1, mean_FONN1,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman'); %set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_1^{NN}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');

title({'$\mathbf{NN}$'},'Interpreter','Latex', 'FontSize', 15);%, 'HorizontalAlignment', 'left');


%% NN property 2
subplot(2,5,9);

%% ART
plot(time_budget1, min_ARTNN2, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTNN2, 'Color', Color(1,:));
q1 = patch([time_budget1 fliplr(time_budget1)], [min_ARTNN2 fliplr(max_ARTNN2)], Color(1,:));
q1.LineWidth = 1;
q1.EdgeColor = Color(1,:);
alpha(0.2)
h_art1 = plot(time_budget1, mean_ARTNN2,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTNN2, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTNN2, 'Color', Color(2,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FTNN2 fliplr(max_FTNN2)], Color(2,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(2,:);
alpha(0.2)
h_ft1 = plot(time_budget1, mean_FTNN2,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODNN2, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODNN2, 'Color', Color(3,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_VBODNN2 fliplr(max_VBODNN2)], Color(3,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod1 = plot(time_budget1, mean_VBODNN2,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD

plot(time_budget1, min_FBODNN2, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODNN2, 'Color', Color(4,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FBODNN2 fliplr(max_FBODNN2)], Color(4,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod1 = plot(time_budget1, mean_FBODNN2,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCNN2, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCNN2, 'Color', Color(5,:));
q1 = patch([time_budget1 fliplr(time_budget1)], [min_FCNN2 fliplr(max_FCNN2)], Color(5,:));
q1.LineWidth = 1;
q1.EdgeColor = Color(5,:);
alpha(0.2)
h_fc1 = plot(time_budget1, mean_FCNN2,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FONN2, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FONN2, 'Color', Color(6,:), 'LineStyle', "-.");
q1 = patch([time_budget1 fliplr(time_budget1)], [min_FONN2 fliplr(max_FONN2)], Color(6,:), 'LineStyle', "-.");
q1.LineWidth = 1;
q1.EdgeColor = Color(6,:);
alpha(0.2)
h_fo1 = plot(time_budget1, mean_FONN2,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_2^{NN}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');

title({'$\mathbf{NN}$'},'Interpreter','Latex', 'FontSize', 15);%, 'HorizontalAlignment', 'left');


%% SC
% MT SC
%% Plots for mutation testing w.r.t. property 1

subplot(2,5,10);

%% ART
plot(time_budget1, min_ARTSC, 'Color', Color(1,:));
hold on
plot(time_budget1, max_ARTSC, 'Color', Color(1,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_ARTSC fliplr(max_ARTSC)], Color(1,:));
q.LineWidth = 1;
q.EdgeColor = Color(1,:);
alpha(0.2)
h_art = plot(time_budget1, mean_ARTSC,'LineWidth', 2, 'Color', Color(1,:));
alpha(0.2)
hold on

%% FT
plot(time_budget1, min_FTSC, 'Color', Color(2,:));
hold on
plot(time_budget1, max_FTSC, 'Color', Color(2,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_FTSC fliplr(max_FTSC)], Color(2,:));
p.LineWidth = 1;
p.EdgeColor = Color(2,:);
alpha(0.2)
h_ft = plot(time_budget1, mean_FTSC,'LineWidth', 2, 'Color', Color(2,:));
alpha(0.2)
hold on

%% VB-OD
plot(time_budget1, min_VBODSC, 'Color', Color(3,:));
hold on
plot(time_budget1, max_VBODSC, 'Color', Color(3,:));
p = patch([time_budget1 fliplr(time_budget1)], [min_VBODSC fliplr(max_VBODSC)], Color(3,:));
p.LineWidth = 1;
p.EdgeColor = Color(3,:);
alpha(0.2)
h_vbod = plot(time_budget1, mean_VBODSC,'LineWidth', 2, 'Color', Color(3,:));
alpha(0.2)
hold on


%% FB-OD
plot(time_budget1, min_FBODSC, 'Color', Color(4,:));
hold on
plot(time_budget1, max_FBODSC, 'Color', Color(4,:));
p1 = patch([time_budget1 fliplr(time_budget1)], [min_FBODSC fliplr(max_FBODSC)], Color(4,:));
p1.LineWidth = 1;
p1.EdgeColor = Color(4,:);
alpha(0.2)
h_fbod = plot(time_budget1, mean_FBODSC,'LineWidth', 2, 'Color', Color(4,:));
alpha(0.2)
hold on

%% FC
plot(time_budget1, min_FCSC, 'Color', Color(5,:));
hold on
plot(time_budget1, max_FCSC, 'Color', Color(5,:));
q = patch([time_budget1 fliplr(time_budget1)], [min_FCSC fliplr(max_FCSC)], Color(5,:));
q.LineWidth = 1;
q.EdgeColor = Color(5,:);
alpha(0.2)
h_fc = plot(time_budget1, mean_FCSC,'LineWidth', 2, 'Color', Color(5,:));
alpha(0.2)
hold on

%% FO
plot(time_budget1, min_FOSC, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
plot(time_budget1, max_FOSC, 'Color', Color(6,:), 'LineStyle', "-.");
q = patch([time_budget1 fliplr(time_budget1)], [min_FOSC fliplr(max_FOSC)], Color(6,:), 'LineStyle', "-.");
q.LineWidth = 1;
q.EdgeColor = Color(6,:);
alpha(0.2)
h_fo = plot(time_budget1, mean_FOSC,'LineWidth', 2, 'Color', Color(6,:), 'LineStyle', "-.");
alpha(0.2)

set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_1^{SC}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');

title({'$\mathbf{SC}$'},'Interpreter','Latex', 'FontSize', 15);


%% Overall Legend
% Construct a Legend with the data from the sub-plots
leg = legend([h_art1 h_ft1 h_vbod1 h_fbod1 h_fc1 h_fo1],'ART','FT','OD-VB', 'OD-FB', 'FCT-C','FCT-S','AutoUpdate','off', 'orientation','horizontal', 'Location','south');

