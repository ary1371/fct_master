%% Mean PBMT trends: mean number of mutants killed for all models for
% different test generation strategies

% Coloring sequence: ART, FT, VB-OD, FB-OD, FC (FCT-C), FO (FCT-S)

%% Load the result data

load('PBMTmeandata.mat');
load('PBMTmeanFCS.mat');
load('PBMTmeanATCS.mat');
load('PBMTmeanAECS.mat');
load('PBMTmeanNN.mat');
load('PBMTmeanSC.mat');

%% Plotting
figure;

%% FCS property 1

subplot(2,5,1);
h_art1 = plot(time_budget1, mean_ARTFCS1,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft1 = plot(time_budget1, mean_FTFCS1,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod1 = plot(time_budget1, mean_VBODFCS1,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod1 = plot(time_budget1, mean_FBODFCS1,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc1 = plot(time_budget1, mean_FCFCS1,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo1 = plot(time_budget1, mean_FOFCS1,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box
set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_1^{FCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{FCS}$'},'Interpreter','Latex', 'FontSize', 15);

%% FCS Property 2

subplot(2,5,2);
h_art1 = plot(time_budget1, mean_ARTFCS2,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft1 = plot(time_budget1, mean_FTFCS2,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod1 = plot(time_budget1, mean_VBODFCS2,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod1 = plot(time_budget1, mean_FBODFCS2,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc1 = plot(time_budget1, mean_FCFCS2,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo1 = plot(time_budget1, mean_FOFCS2,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');

ylabel({'\boldmath{$|{\phi_2^{FCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{FCS}$'},'Interpreter','Latex', 'FontSize', 15);

%% ATCS property 1

subplot(2,5,3)
h_art = plot(time_budget1, mean_ART,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft = plot(time_budget1, mean_FT,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod = plot(time_budget1, mean_VBOD,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod = plot(time_budget1, mean_FBOD,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc = plot(time_budget1, mean_FC,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo = plot(time_budget1, mean_FO,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'LineWidth',1);
set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');

ylabel({'\boldmath{$|{\phi_1^{ATCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);

%% ATCS property 2

subplot(2,5,4);
h_art1 = plot(time_budget1, mean_ART1,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft1 = plot(time_budget1, mean_FT1,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod1 = plot(time_budget1, mean_VBOD1,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod1 = plot(time_budget1, mean_FBOD1,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc1 = plot(time_budget1, mean_FC1,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo1 = plot(time_budget1, mean_FO1,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');

ylabel({'\boldmath{$|{\phi_2^{ATCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);%, 'HorizontalAlignment', 'left');

%% ATCS Property 3

subplot(2,5,5)
h_art = plot(time_budget1, mean_ART2,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft = plot(time_budget1, mean_FT2,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod = plot(time_budget1, mean_VBOD2,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod = plot(time_budget1, mean_FBOD2,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc = plot(time_budget1, mean_FC2,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo = plot(time_budget1, mean_FO2,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');

ylabel({'\boldmath{$|{\phi_3^{ATCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);

%% PROPERTY 4

subplot(2,5,6);
h_art1 = plot(time_budget1, mean_ART3,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft1 = plot(time_budget1, mean_FT3,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod1 = plot(time_budget1, mean_VBOD3,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod1 = plot(time_budget1, mean_FBOD3,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc1 = plot(time_budget1, mean_FC3,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo1 = plot(time_budget1, mean_FO3,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_4^{ATCS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{ATCS}$'},'Interpreter','Latex', 'FontSize', 15);


%% AECS property 1

subplot(2,5,7);
h_art1 = plot(time_budget1, mean_ARTAECS,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft1 = plot(time_budget1, mean_FTAECS,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod1 = plot(time_budget1, mean_VBODAECS,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod1 = plot(time_budget1, mean_FBODAECS,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc1 = plot(time_budget1, mean_FCAECS,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo1 = plot(time_budget1, mean_FOAECS,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_1^{AECS}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{AECS}$'},'Interpreter','Latex', 'FontSize', 15);


%% NN property1

subplot(2,5,8);
h_art1 = plot(time_budget1, mean_ARTNN1,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft1 = plot(time_budget1, mean_FTNN1,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod1 = plot(time_budget1, mean_VBODNN1,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod1 = plot(time_budget1, mean_FBODNN1,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc1 = plot(time_budget1, mean_FCNN1,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo1 = plot(time_budget1, mean_FONN1,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman');
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');

ylabel({'\boldmath{$|{\phi_1^{NN}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{NN}$'},'Interpreter','Latex', 'FontSize', 15);


%% NN property 2
subplot(2,5,9);
h_art1 = plot(time_budget1, mean_ARTNN2,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft1 = plot(time_budget1, mean_FTNN2,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod1 = plot(time_budget1, mean_VBODNN2,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod1 = plot(time_budget1, mean_FBODNN2,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc1 = plot(time_budget1, mean_FCNN2,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo1 = plot(time_budget1, mean_FONN2,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');
ylabel({'\boldmath{$|{\phi_2^{NN}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{NN}$'},'Interpreter','Latex', 'FontSize', 15);


%% SC property 1
subplot(2,5,10);
h_art1 = plot(time_budget1, mean_ARTSC,'LineWidth', 2.5, 'Color', Color(1,:));
hold on
h_ft1 = plot(time_budget1, mean_FTSC,'LineWidth', 2.5, 'Color', Color(2,:));
hold on
h_vbod1 = plot(time_budget1, mean_VBODSC,'LineWidth', 2.5, 'Color', Color(3,:));
hold on
h_fbod1 = plot(time_budget1, mean_FBODSC,'LineWidth', 2.5, 'Color', Color(4,:));
hold on
h_fc1 = plot(time_budget1, mean_FCSC,'LineWidth', 2.5, 'Color', Color(5,:));
hold on
h_fo1 = plot(time_budget1, mean_FOSC,'LineWidth', 2.5, 'Color', Color(6,:), 'LineStyle', "-.");
hold on
set(gca,'fontsize',14, 'FontWeight','bold');
set(gca,'LineWidth',1); % width of box

set(gca,'fontname','Times New Roman'); 
xlim([0 20])
xlabel({'\bf{Time ($\mathbf{\times 10^3}$ seconds)}'},'Interpreter','Latex');

ylabel({'\boldmath{$|{\phi_1^{SC}}$}\textbf{-killed mutants}$\mathbf{|}$'},'Interpreter','Latex');
title({'$\mathbf{SC}$'},'Interpreter','Latex', 'FontSize', 15);

% Construct a Legend with the data from the sub-plots
leg = legend([h_art1 h_ft1 h_vbod1 h_fbod1 h_fc1 h_fo1],'ART','FT','OD-VB', 'OD-FB', 'FCT-C','FCT-S','AutoUpdate','off', 'orientation','horizontal', 'Location','south');
