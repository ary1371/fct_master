function[robustness] = monitorAECS(simout)

% FUNCTION: Monitor test outputs wrt specifications and output monitoring
% verdict and robustness using RTAMT
% Created:  17-07-2023

% Import RTAMT Python library
mod = py.importlib.import_module('rtamt');

spec_model = mod.STLSpecification(1);
spec_model.name = 'MyMonitorModel';
declare_var(spec_model, 's426', 'float');
declare_var(spec_model, 's236', 'float');
declare_var(spec_model, 'c', 'float');
spec_model.spec = 'c = historically(rise( s426 >= 0.09) implies (eventually[0:200]always[0:100](abs( s426 - s236 ) <= 0.02)))';
parse(spec_model);
pastify(spec_model);
for i = 1:length(simout)
    model_list = py.list({py.tuple({'s426', simout(i,3)})});
    append(model_list, py.tuple({'s236',simout(i,1)}));
    robustness_model(i) = update(spec_model, i, model_list);
end

robustness = robustness_model(end);
end
