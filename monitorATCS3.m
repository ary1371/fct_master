function[robustness] = monitorATCS3(simout)

% FUNCTION: Monitor test outputs wrt specifications and output monitoring
% verdict and robustness using RTAMT
% Created:  17-07-2023

% Import RTAMT Python library
mod = py.importlib.import_module('rtamt');

spec_model = mod.STLSpecification(1);
spec_model.name = 'MyMonitorModel';
declare_var(spec_model, 's42', 'float');
declare_var(spec_model, 's55', 'float');
declare_var(spec_model, 'c', 'float');
spec_model.spec = 'c = historically((s55 < 4500)) implies (eventually[0:101] (s42 < 120))';
parse(spec_model);
pastify(spec_model);
for i = 1:length(simout)
    model_list = py.list({py.tuple({'s42', simout(i,1)})});
    append(model_list, py.tuple({'s55',simout(i,2)}));
    robustness_model(i) = update(spec_model, i, model_list);
end

robustness = robustness_model(end);
end
