function[robustness] = monitorFTF1(simout)

% FUNCTION: Monitor test outputs wrt specifications and output monitoring
% verdict and robustness using RTAMT

% Import RTAMT Python library
mod = py.importlib.import_module('rtamt');

spec_model = mod.STLSpecification(1);
spec_model.name = 'MyMonitorModel';
declare_var(spec_model, 's70', 'float');
declare_var(spec_model, 'c', 'float');

spec_model.spec = 'c = always[1001:11001](11.68 <= s70 <= 17.52)';

parse(spec_model);
pastify(spec_model);
for i = 1:length(simout)
    model_list = py.list({py.tuple({'s70', simout(i,5)})});
    robustness_model(i) = update(spec_model, i, model_list);
end

robustness = robustness_model(end);
end
