function[robustness] = monitorFTF2(simout)

% FUNCTION: Monitor test outputs wrt specifications and output monitoring
% verdict and robustness using RTAMT

% Import RTAMT Python library
mod = py.importlib.import_module('rtamt');

spec_model = mod.STLSpecification(1);
spec_model.name = 'MyMonitorModel';
declare_var(spec_model, 's93', 'float');
declare_var(spec_model, 'c', 'float');

spec_model.spec = 'c = eventually[0:10001] (always[0:101]( (s93 > 0) or (s93 < 0) ) )';

parse(spec_model);
pastify(spec_model);
for i = 1:length(simout)
    model_list = py.list({py.tuple({'s93', simout(i,61)})});
    robustness_model(i) = update(spec_model, i, model_list);
end

robustness = robustness_model(end);
end
